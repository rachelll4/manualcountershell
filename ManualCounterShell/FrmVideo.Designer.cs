﻿namespace ManualCounterShell
{
    partial class FrmVideo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpBorder = new System.Windows.Forms.TableLayoutPanel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnSaveDefault = new System.Windows.Forms.Button();
            this.tlpWhole = new System.Windows.Forms.TableLayoutPanel();
            this.tlpLeft = new System.Windows.Forms.TableLayoutPanel();
            this.lbVideos = new System.Windows.Forms.ListBox();
            this.tlpRight = new System.Windows.Forms.TableLayoutPanel();
            this.lblPixelCheck = new System.Windows.Forms.Label();
            this.btnFast = new System.Windows.Forms.Button();
            this.btnSlow = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.lblData = new System.Windows.Forms.Label();
            this.pbVideo = new System.Windows.Forms.PictureBox();
            this.lblCOL = new System.Windows.Forms.Label();
            this.lblHeading = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.tlpBorder.SuspendLayout();
            this.tlpWhole.SuspendLayout();
            this.tlpLeft.SuspendLayout();
            this.tlpRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbVideo)).BeginInit();
            this.SuspendLayout();
            // 
            // tlpBorder
            // 
            this.tlpBorder.BackColor = System.Drawing.Color.LightSlateGray;
            this.tlpBorder.ColumnCount = 7;
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tlpBorder.Controls.Add(this.btnSave, 3, 2);
            this.tlpBorder.Controls.Add(this.btnSaveDefault, 4, 2);
            this.tlpBorder.Controls.Add(this.tlpWhole, 1, 1);
            this.tlpBorder.Controls.Add(this.lblCOL, 1, 0);
            this.tlpBorder.Controls.Add(this.lblHeading, 2, 0);
            this.tlpBorder.Controls.Add(this.btnBack, 5, 2);
            this.tlpBorder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpBorder.Location = new System.Drawing.Point(0, 0);
            this.tlpBorder.Name = "tlpBorder";
            this.tlpBorder.RowCount = 3;
            this.tlpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tlpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpBorder.Size = new System.Drawing.Size(994, 628);
            this.tlpBorder.TabIndex = 3;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Thistle;
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSave.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Navy;
            this.btnSave.Location = new System.Drawing.Point(687, 567);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(93, 58);
            this.btnSave.TabIndex = 25;
            this.btnSave.Text = "Save settings";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSaveDefault
            // 
            this.btnSaveDefault.BackColor = System.Drawing.Color.Thistle;
            this.btnSaveDefault.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSaveDefault.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveDefault.ForeColor = System.Drawing.Color.Navy;
            this.btnSaveDefault.Location = new System.Drawing.Point(786, 567);
            this.btnSaveDefault.Name = "btnSaveDefault";
            this.btnSaveDefault.Size = new System.Drawing.Size(93, 58);
            this.btnSaveDefault.TabIndex = 24;
            this.btnSaveDefault.Text = "Save to defaults";
            this.btnSaveDefault.UseVisualStyleBackColor = false;
            this.btnSaveDefault.Click += new System.EventHandler(this.btnSaveDefault_Click);
            // 
            // tlpWhole
            // 
            this.tlpWhole.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tlpWhole.ColumnCount = 2;
            this.tlpBorder.SetColumnSpan(this.tlpWhole, 5);
            this.tlpWhole.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tlpWhole.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tlpWhole.Controls.Add(this.tlpLeft, 0, 0);
            this.tlpWhole.Controls.Add(this.tlpRight, 1, 0);
            this.tlpWhole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpWhole.Location = new System.Drawing.Point(12, 65);
            this.tlpWhole.Name = "tlpWhole";
            this.tlpWhole.RowCount = 1;
            this.tlpWhole.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpWhole.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 838F));
            this.tlpWhole.Size = new System.Drawing.Size(966, 496);
            this.tlpWhole.TabIndex = 7;
            // 
            // tlpLeft
            // 
            this.tlpLeft.ColumnCount = 1;
            this.tlpLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpLeft.Controls.Add(this.lbVideos, 0, 0);
            this.tlpLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpLeft.Location = new System.Drawing.Point(3, 3);
            this.tlpLeft.Name = "tlpLeft";
            this.tlpLeft.RowCount = 4;
            this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpLeft.Size = new System.Drawing.Size(525, 490);
            this.tlpLeft.TabIndex = 0;
            // 
            // lbVideos
            // 
            this.lbVideos.BackColor = System.Drawing.Color.Thistle;
            this.lbVideos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbVideos.Font = new System.Drawing.Font("Verdana", 16F);
            this.lbVideos.ForeColor = System.Drawing.Color.Navy;
            this.lbVideos.FormattingEnabled = true;
            this.lbVideos.ItemHeight = 32;
            this.lbVideos.Location = new System.Drawing.Point(3, 3);
            this.lbVideos.Name = "lbVideos";
            this.tlpLeft.SetRowSpan(this.lbVideos, 4);
            this.lbVideos.Size = new System.Drawing.Size(519, 484);
            this.lbVideos.TabIndex = 0;
            this.lbVideos.SelectedIndexChanged += new System.EventHandler(this.lbVideos_OnSelectedIndexChanged);
            // 
            // tlpRight
            // 
            this.tlpRight.ColumnCount = 4;
            this.tlpRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpRight.Controls.Add(this.lblPixelCheck, 0, 6);
            this.tlpRight.Controls.Add(this.btnFast, 3, 2);
            this.tlpRight.Controls.Add(this.btnSlow, 0, 2);
            this.tlpRight.Controls.Add(this.btnPause, 2, 1);
            this.tlpRight.Controls.Add(this.btnPlay, 1, 1);
            this.tlpRight.Controls.Add(this.lblData, 0, 4);
            this.tlpRight.Controls.Add(this.pbVideo, 0, 5);
            this.tlpRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpRight.Location = new System.Drawing.Point(534, 3);
            this.tlpRight.Name = "tlpRight";
            this.tlpRight.RowCount = 7;
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2F));
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3F));
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3F));
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66F));
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4F));
            this.tlpRight.Size = new System.Drawing.Size(429, 490);
            this.tlpRight.TabIndex = 1;
            // 
            // lblPixelCheck
            // 
            this.lblPixelCheck.AutoSize = true;
            this.tlpRight.SetColumnSpan(this.lblPixelCheck, 4);
            this.lblPixelCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPixelCheck.Font = new System.Drawing.Font("Verdana", 12F);
            this.lblPixelCheck.ForeColor = System.Drawing.Color.Thistle;
            this.lblPixelCheck.Location = new System.Drawing.Point(3, 467);
            this.lblPixelCheck.Name = "lblPixelCheck";
            this.lblPixelCheck.Size = new System.Drawing.Size(423, 23);
            this.lblPixelCheck.TabIndex = 29;
            this.lblPixelCheck.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnFast
            // 
            this.btnFast.BackColor = System.Drawing.Color.Thistle;
            this.btnFast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnFast.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold);
            this.btnFast.ForeColor = System.Drawing.Color.Navy;
            this.btnFast.Location = new System.Drawing.Point(324, 26);
            this.btnFast.Name = "btnFast";
            this.btnFast.Size = new System.Drawing.Size(102, 52);
            this.btnFast.TabIndex = 28;
            this.btnFast.Text = "Faster";
            this.btnFast.UseVisualStyleBackColor = false;
            this.btnFast.Click += new System.EventHandler(this.btnFast_Click);
            // 
            // btnSlow
            // 
            this.btnSlow.BackColor = System.Drawing.Color.Thistle;
            this.btnSlow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSlow.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold);
            this.btnSlow.ForeColor = System.Drawing.Color.Navy;
            this.btnSlow.Location = new System.Drawing.Point(3, 26);
            this.btnSlow.Name = "btnSlow";
            this.btnSlow.Size = new System.Drawing.Size(101, 52);
            this.btnSlow.TabIndex = 27;
            this.btnSlow.Text = "Slower";
            this.btnSlow.UseVisualStyleBackColor = false;
            this.btnSlow.Click += new System.EventHandler(this.btnSlow_Click);
            // 
            // btnPause
            // 
            this.btnPause.BackColor = System.Drawing.Color.Thistle;
            this.btnPause.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPause.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold);
            this.btnPause.ForeColor = System.Drawing.Color.Navy;
            this.btnPause.Location = new System.Drawing.Point(217, 12);
            this.btnPause.Name = "btnPause";
            this.tlpRight.SetRowSpan(this.btnPause, 3);
            this.btnPause.Size = new System.Drawing.Size(101, 80);
            this.btnPause.TabIndex = 25;
            this.btnPause.Text = "Pause";
            this.btnPause.UseVisualStyleBackColor = false;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.BackColor = System.Drawing.Color.Thistle;
            this.btnPlay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPlay.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold);
            this.btnPlay.ForeColor = System.Drawing.Color.Navy;
            this.btnPlay.Location = new System.Drawing.Point(110, 12);
            this.btnPlay.Name = "btnPlay";
            this.tlpRight.SetRowSpan(this.btnPlay, 3);
            this.btnPlay.Size = new System.Drawing.Size(101, 80);
            this.btnPlay.TabIndex = 24;
            this.btnPlay.Text = "Play";
            this.btnPlay.UseVisualStyleBackColor = false;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.tlpRight.SetColumnSpan(this.lblData, 4);
            this.lblData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblData.Font = new System.Drawing.Font("Verdana", 12F);
            this.lblData.ForeColor = System.Drawing.Color.Thistle;
            this.lblData.Location = new System.Drawing.Point(3, 95);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(423, 49);
            this.lblData.TabIndex = 22;
            this.lblData.Text = "Select video.";
            this.lblData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbVideo
            // 
            this.pbVideo.BackColor = System.Drawing.Color.Black;
            this.tlpRight.SetColumnSpan(this.pbVideo, 4);
            this.pbVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbVideo.Location = new System.Drawing.Point(3, 147);
            this.pbVideo.Name = "pbVideo";
            this.pbVideo.Size = new System.Drawing.Size(423, 317);
            this.pbVideo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbVideo.TabIndex = 26;
            this.pbVideo.TabStop = false;
            // 
            // lblCOL
            // 
            this.lblCOL.AutoSize = true;
            this.lblCOL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCOL.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCOL.ForeColor = System.Drawing.Color.Black;
            this.lblCOL.Location = new System.Drawing.Point(12, 0);
            this.lblCOL.Name = "lblCOL";
            this.lblCOL.Size = new System.Drawing.Size(93, 62);
            this.lblCOL.TabIndex = 6;
            this.lblCOL.Text = "COL";
            this.lblCOL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.tlpBorder.SetColumnSpan(this.lblHeading, 3);
            this.lblHeading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHeading.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.ForeColor = System.Drawing.Color.Black;
            this.lblHeading.Location = new System.Drawing.Point(111, 0);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(768, 62);
            this.lblHeading.TabIndex = 5;
            this.lblHeading.Text = "Manual Counter Workstation";
            this.lblHeading.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.Thistle;
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBack.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.ForeColor = System.Drawing.Color.Navy;
            this.btnBack.Location = new System.Drawing.Point(885, 567);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(93, 58);
            this.btnBack.TabIndex = 23;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // FrmVideo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(994, 628);
            this.Controls.Add(this.tlpBorder);
            this.Name = "FrmVideo";
            this.Text = "Form1";
            this.tlpBorder.ResumeLayout(false);
            this.tlpBorder.PerformLayout();
            this.tlpWhole.ResumeLayout(false);
            this.tlpLeft.ResumeLayout(false);
            this.tlpRight.ResumeLayout(false);
            this.tlpRight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbVideo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpBorder;
        private System.Windows.Forms.TableLayoutPanel tlpWhole;
        private System.Windows.Forms.TableLayoutPanel tlpLeft;
        private System.Windows.Forms.ListBox lbVideos;
        private System.Windows.Forms.TableLayoutPanel tlpRight;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.PictureBox pbVideo;
        private System.Windows.Forms.Label lblCOL;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnFast;
        private System.Windows.Forms.Button btnSlow;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnSaveDefault;
        private System.Windows.Forms.Label lblPixelCheck;
    }
}


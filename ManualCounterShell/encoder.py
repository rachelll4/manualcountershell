import csv

with open("paramData.txt",mode='r') as csv_file:
    string = csv_file.read()
    key = 'a8cc75d'
encoded_chars = []
for i in range(len(string)):
    key_c = key[i % len(key)]
    encoded_c = chr(ord(string[i]) + ord(key_c) % 256)
    encoded_chars.append(encoded_c)
encoded_string = "".join(encoded_chars)
with open("secureFile.txt",'wb') as csv_file:
    csv_file.write(encoded_string.encode())
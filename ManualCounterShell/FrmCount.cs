﻿//Author: Rachel Logan
//Date: 5/23/19

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using ZeroMQ;
using System.Diagnostics;

namespace ManualCounterShell
{
    public partial class FrmCount : Form
    {
        ZContext ctx;
        ZSocket receiver;
        ZSocket countListener;
        bool zmqOpen;
        Thread poller;
        bool polling;
        StreamWriter sw;

        public FrmCount()
        {
            InitializeComponent();
            btnBack.Enabled = false;

            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            lblCount.Focus();
            btnReset.Enabled = false;
            polling = false;
            sw = new StreamWriter("output.txt");
            Thread initialize = new Thread(initializeCounter);
            initialize.Start();
            
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "Saving video";
            lblStatus.Update();
            btnBack.Enabled = false;
            btnReset.Enabled = false;
            polling = false;
            
            receiver.SendFrame(new ZFrame("1")); //save
            sw.WriteLine("saving: "+receiver.ReceiveFrame().ReadString()); //this is 'done' or 'no vid to save'

            lblStatus.Text = "Re-initializing...";

            sw.Flush();
            receiver.SendFrame(new ZFrame("2")); //stop this polling process
            //need more fluff here or diff thread
            string msg=receiver.ReceiveFrame().ReadString();
            sw.WriteLine("restart received: "+msg);
            lblCount.Text = "Count: 0";
            lblCount.Focus();
            if (msg == "ready")
            {
                lblStatus.Text = "Ready to count.";
                btnBack.Enabled = true;
                btnReset.Enabled = true;
                polling = true;
            }
            else
            {
                lblStatus.Text = "Not initialized: " + msg;
                polling = false;
                btnBack.Enabled = true;
            }
        }

        private void initializeCounter()
        {
            ZMQinit();

            //deal with start message: will either send "ready" or "not ready"
            string msg = receiver.ReceiveFrame().ReadString();
            if (msg == "ready")
            {
                lblStatus.Text = "Ready to count.";
                btnReset.Enabled = true;
                polling = true;
            }
            else
            {
                lblStatus.Text = "Not initialized: "+msg;
                btnBack.Enabled = true;
                btnReset.Enabled = false;
            }
                
            sw.Close();
            poller = new Thread(watchPython);
            poller.Start();
            btnBack.Enabled = true;
        }

        private void watchPython()
        {
            sw = new StreamWriter("output.txt",true);
            sw.WriteLine("poller started");
            //here, start poller for errors
            ZMessage pymsg;
            ZError zerror;

            var poll = ZPollItem.CreateReceiver();

            while (true)
            {
                if (polling)
                {
                    sw.Flush();
                    if (countListener.PollIn(poll, out pymsg, out zerror, TimeSpan.FromMilliseconds(20)))
                    {
                        string message = pymsg.PopString();
                        char index = message[0];
                        string communication = message.Substring(2);
                        sw.WriteLine(communication);
                        if (index == '0') //then it's a count
                            lblCount.BeginInvoke((MethodInvoker)delegate { lblCount.Text = "Count: " + communication; });
                        else if (index=='1') //show-stopping error
                        {
                            lblStatus.BeginInvoke((MethodInvoker)delegate { lblStatus.Text = message; btnBack.Visible = true; });
                            
                        }
                        else //warning, show goes on
                        {
                            lblStatus.BeginInvoke((MethodInvoker)delegate { lblStatus.Text = message; });
                            //polling = false;
                        }
                    }
                    else
                    {
                        if (zerror.Number!=11) sw.WriteLine("Error: " + zerror.Text + " " + zerror.Number);
                        Thread.Sleep(60);
                    }
                }
                else
                {
                    Thread.Sleep(10);
                }
            }
        }

        private void ZMQinit()
        {
            ctx = new ZContext();
            receiver = new ZSocket(ctx, ZSocketType.PAIR);
            //bind socket
            receiver.Connect("tcp://127.0.0.1:5556");
            sw.WriteLine("receiver connected");

            //this is the socket used for counting in the poller thread
            countListener = new ZSocket(ctx, ZSocketType.PAIR);
            countListener.Bind("tcp://127.0.0.1:5557");
            sw.WriteLine("countlistener bound");

            receiver.Send(new ZFrame("both connected"));
            sw.WriteLine("new poller socket: " + countListener.ReceiveFrame().ReadString());

            lblStatus.BeginInvoke((MethodInvoker)delegate { lblStatus.Text = "Connected. Initializing..."; });
            zmqOpen = true;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            receiver.SendFrame(new ZFrame("3")); //tells python to disconnect
            receiver.ReceiveFrame();
            polling = false;
            if (poller != null && poller.IsAlive) poller.Abort();
            if (zmqOpen)
            {
                countListener.Dispose();
                receiver.Dispose();
                ctx.Dispose();
            }
            sw.Close();
            (new FrmStart()).Show();
            this.Hide();
        }
    }
}

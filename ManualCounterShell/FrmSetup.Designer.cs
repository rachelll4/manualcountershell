﻿namespace ManualCounterShell
{
    partial class FrmSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpBorder = new System.Windows.Forms.TableLayoutPanel();
            this.tlpWhole = new System.Windows.Forms.TableLayoutPanel();
            this.tlpLeft = new System.Windows.Forms.TableLayoutPanel();
            this.lbSettings = new System.Windows.Forms.ListBox();
            this.tlpRight = new System.Windows.Forms.TableLayoutPanel();
            this.btnBkSp = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btnGo = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btnChange = new System.Windows.Forms.Button();
            this.btnDefault = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblEntered = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.lblCOL = new System.Windows.Forms.Label();
            this.lblHeading = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblAuth = new System.Windows.Forms.Label();
            this.tlpBorder.SuspendLayout();
            this.tlpWhole.SuspendLayout();
            this.tlpLeft.SuspendLayout();
            this.tlpRight.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpBorder
            // 
            this.tlpBorder.BackColor = System.Drawing.Color.LightSlateGray;
            this.tlpBorder.ColumnCount = 6;
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tlpBorder.Controls.Add(this.lblAuth, 3, 0);
            this.tlpBorder.Controls.Add(this.tlpWhole, 1, 1);
            this.tlpBorder.Controls.Add(this.lblCOL, 1, 0);
            this.tlpBorder.Controls.Add(this.lblHeading, 2, 0);
            this.tlpBorder.Controls.Add(this.btnBack, 4, 2);
            this.tlpBorder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpBorder.Location = new System.Drawing.Point(0, 0);
            this.tlpBorder.Name = "tlpBorder";
            this.tlpBorder.RowCount = 3;
            this.tlpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tlpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.Size = new System.Drawing.Size(981, 569);
            this.tlpBorder.TabIndex = 3;
            // 
            // tlpWhole
            // 
            this.tlpWhole.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tlpWhole.ColumnCount = 2;
            this.tlpBorder.SetColumnSpan(this.tlpWhole, 4);
            this.tlpWhole.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tlpWhole.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tlpWhole.Controls.Add(this.tlpLeft, 0, 0);
            this.tlpWhole.Controls.Add(this.tlpRight, 1, 0);
            this.tlpWhole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpWhole.Location = new System.Drawing.Point(12, 59);
            this.tlpWhole.Name = "tlpWhole";
            this.tlpWhole.RowCount = 1;
            this.tlpWhole.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpWhole.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 838F));
            this.tlpWhole.Size = new System.Drawing.Size(954, 449);
            this.tlpWhole.TabIndex = 7;
            // 
            // tlpLeft
            // 
            this.tlpLeft.ColumnCount = 1;
            this.tlpLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpLeft.Controls.Add(this.lbSettings, 0, 0);
            this.tlpLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpLeft.Location = new System.Drawing.Point(3, 3);
            this.tlpLeft.Name = "tlpLeft";
            this.tlpLeft.RowCount = 4;
            this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpLeft.Size = new System.Drawing.Size(518, 443);
            this.tlpLeft.TabIndex = 0;
            // 
            // lbSettings
            // 
            this.lbSettings.BackColor = System.Drawing.Color.Thistle;
            this.lbSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbSettings.Font = new System.Drawing.Font("Verdana", 16F);
            this.lbSettings.ForeColor = System.Drawing.Color.Navy;
            this.lbSettings.FormattingEnabled = true;
            this.lbSettings.ItemHeight = 32;
            this.lbSettings.Location = new System.Drawing.Point(3, 3);
            this.lbSettings.Name = "lbSettings";
            this.tlpLeft.SetRowSpan(this.lbSettings, 4);
            this.lbSettings.Size = new System.Drawing.Size(512, 437);
            this.lbSettings.TabIndex = 0;
            this.lbSettings.SelectedIndexChanged += new System.EventHandler(this.lbSettings_OnSelectedIndexChanged);
            // 
            // tlpRight
            // 
            this.tlpRight.ColumnCount = 4;
            this.tlpRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tlpRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tlpRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tlpRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tlpRight.Controls.Add(this.btnBkSp, 1, 6);
            this.tlpRight.Controls.Add(this.btn0, 2, 6);
            this.tlpRight.Controls.Add(this.btnGo, 3, 6);
            this.tlpRight.Controls.Add(this.btn7, 1, 5);
            this.tlpRight.Controls.Add(this.btn8, 2, 5);
            this.tlpRight.Controls.Add(this.btn9, 3, 5);
            this.tlpRight.Controls.Add(this.btn4, 1, 4);
            this.tlpRight.Controls.Add(this.btn5, 2, 4);
            this.tlpRight.Controls.Add(this.btn6, 3, 4);
            this.tlpRight.Controls.Add(this.btn1, 1, 3);
            this.tlpRight.Controls.Add(this.btn2, 2, 3);
            this.tlpRight.Controls.Add(this.btn3, 3, 3);
            this.tlpRight.Controls.Add(this.btnChange, 1, 0);
            this.tlpRight.Controls.Add(this.btnDefault, 2, 0);
            this.tlpRight.Controls.Add(this.btnCancel, 3, 0);
            this.tlpRight.Controls.Add(this.lblEntered, 1, 2);
            this.tlpRight.Controls.Add(this.lblData, 1, 1);
            this.tlpRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpRight.Location = new System.Drawing.Point(527, 3);
            this.tlpRight.Name = "tlpRight";
            this.tlpRight.RowCount = 7;
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22F));
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14F));
            this.tlpRight.Size = new System.Drawing.Size(424, 443);
            this.tlpRight.TabIndex = 1;
            // 
            // btnBkSp
            // 
            this.btnBkSp.BackColor = System.Drawing.Color.Thistle;
            this.btnBkSp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBkSp.Font = new System.Drawing.Font("Verdana", 25F, System.Drawing.FontStyle.Bold);
            this.btnBkSp.ForeColor = System.Drawing.Color.Navy;
            this.btnBkSp.Location = new System.Drawing.Point(7, 383);
            this.btnBkSp.Name = "btnBkSp";
            this.btnBkSp.Size = new System.Drawing.Size(133, 57);
            this.btnBkSp.TabIndex = 7;
            this.btnBkSp.Text = "<";
            this.btnBkSp.UseVisualStyleBackColor = false;
            this.btnBkSp.Click += new System.EventHandler(this.btnBkSp_Click);
            // 
            // btn0
            // 
            this.btn0.BackColor = System.Drawing.Color.Thistle;
            this.btn0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn0.Font = new System.Drawing.Font("Verdana", 25F, System.Drawing.FontStyle.Bold);
            this.btn0.ForeColor = System.Drawing.Color.Navy;
            this.btn0.Location = new System.Drawing.Point(146, 383);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(133, 57);
            this.btn0.TabIndex = 11;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = false;
            this.btn0.Click += new System.EventHandler(this.btn0_Click);
            // 
            // btnGo
            // 
            this.btnGo.BackColor = System.Drawing.Color.Thistle;
            this.btnGo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGo.Font = new System.Drawing.Font("Verdana", 25F, System.Drawing.FontStyle.Bold);
            this.btnGo.ForeColor = System.Drawing.Color.Navy;
            this.btnGo.Location = new System.Drawing.Point(285, 383);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(136, 57);
            this.btnGo.TabIndex = 10;
            this.btnGo.Text = "Go";
            this.btnGo.UseVisualStyleBackColor = false;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // btn7
            // 
            this.btn7.BackColor = System.Drawing.Color.Thistle;
            this.btn7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn7.Font = new System.Drawing.Font("Verdana", 25F, System.Drawing.FontStyle.Bold);
            this.btn7.ForeColor = System.Drawing.Color.Navy;
            this.btn7.Location = new System.Drawing.Point(7, 321);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(133, 56);
            this.btn7.TabIndex = 6;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = false;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn8
            // 
            this.btn8.BackColor = System.Drawing.Color.Thistle;
            this.btn8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn8.Font = new System.Drawing.Font("Verdana", 25F, System.Drawing.FontStyle.Bold);
            this.btn8.ForeColor = System.Drawing.Color.Navy;
            this.btn8.Location = new System.Drawing.Point(146, 321);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(133, 56);
            this.btn8.TabIndex = 8;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = false;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn9
            // 
            this.btn9.BackColor = System.Drawing.Color.Thistle;
            this.btn9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn9.Font = new System.Drawing.Font("Verdana", 25F, System.Drawing.FontStyle.Bold);
            this.btn9.ForeColor = System.Drawing.Color.Navy;
            this.btn9.Location = new System.Drawing.Point(285, 321);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(136, 56);
            this.btn9.TabIndex = 9;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = false;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn4
            // 
            this.btn4.BackColor = System.Drawing.Color.Thistle;
            this.btn4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn4.Font = new System.Drawing.Font("Verdana", 25F, System.Drawing.FontStyle.Bold);
            this.btn4.ForeColor = System.Drawing.Color.Navy;
            this.btn4.Location = new System.Drawing.Point(7, 259);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(133, 56);
            this.btn4.TabIndex = 16;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = false;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn5
            // 
            this.btn5.BackColor = System.Drawing.Color.Thistle;
            this.btn5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn5.Font = new System.Drawing.Font("Verdana", 25F, System.Drawing.FontStyle.Bold);
            this.btn5.ForeColor = System.Drawing.Color.Navy;
            this.btn5.Location = new System.Drawing.Point(146, 259);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(133, 56);
            this.btn5.TabIndex = 14;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = false;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn6
            // 
            this.btn6.BackColor = System.Drawing.Color.Thistle;
            this.btn6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn6.Font = new System.Drawing.Font("Verdana", 25F, System.Drawing.FontStyle.Bold);
            this.btn6.ForeColor = System.Drawing.Color.Navy;
            this.btn6.Location = new System.Drawing.Point(285, 259);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(136, 56);
            this.btn6.TabIndex = 15;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = false;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.Color.Thistle;
            this.btn1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn1.Font = new System.Drawing.Font("Verdana", 25F, System.Drawing.FontStyle.Bold);
            this.btn1.ForeColor = System.Drawing.Color.Navy;
            this.btn1.Location = new System.Drawing.Point(7, 197);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(133, 56);
            this.btn1.TabIndex = 13;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.Color.Thistle;
            this.btn2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn2.Font = new System.Drawing.Font("Verdana", 25F, System.Drawing.FontStyle.Bold);
            this.btn2.ForeColor = System.Drawing.Color.Navy;
            this.btn2.Location = new System.Drawing.Point(146, 197);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(133, 56);
            this.btn2.TabIndex = 12;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn3
            // 
            this.btn3.BackColor = System.Drawing.Color.Thistle;
            this.btn3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn3.Font = new System.Drawing.Font("Verdana", 25F, System.Drawing.FontStyle.Bold);
            this.btn3.ForeColor = System.Drawing.Color.Navy;
            this.btn3.Location = new System.Drawing.Point(285, 197);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(136, 56);
            this.btn3.TabIndex = 17;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = false;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btnChange
            // 
            this.btnChange.BackColor = System.Drawing.Color.Thistle;
            this.btnChange.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnChange.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold);
            this.btnChange.ForeColor = System.Drawing.Color.Navy;
            this.btnChange.Location = new System.Drawing.Point(7, 3);
            this.btnChange.Name = "btnChange";
            this.btnChange.Size = new System.Drawing.Size(133, 47);
            this.btnChange.TabIndex = 20;
            this.btnChange.Text = "Change";
            this.btnChange.UseVisualStyleBackColor = false;
            this.btnChange.Click += new System.EventHandler(this.btnChange_Click);
            // 
            // btnDefault
            // 
            this.btnDefault.BackColor = System.Drawing.Color.Thistle;
            this.btnDefault.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDefault.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold);
            this.btnDefault.ForeColor = System.Drawing.Color.Navy;
            this.btnDefault.Location = new System.Drawing.Point(146, 3);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(133, 47);
            this.btnDefault.TabIndex = 19;
            this.btnDefault.Text = "Default";
            this.btnDefault.UseVisualStyleBackColor = false;
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Thistle;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold);
            this.btnCancel.ForeColor = System.Drawing.Color.Navy;
            this.btnCancel.Location = new System.Drawing.Point(285, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(136, 47);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblEntered
            // 
            this.lblEntered.AutoSize = true;
            this.tlpRight.SetColumnSpan(this.lblEntered, 3);
            this.lblEntered.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEntered.Font = new System.Drawing.Font("Verdana", 15F, System.Drawing.FontStyle.Bold);
            this.lblEntered.ForeColor = System.Drawing.Color.Thistle;
            this.lblEntered.Location = new System.Drawing.Point(7, 150);
            this.lblEntered.Name = "lblEntered";
            this.lblEntered.Size = new System.Drawing.Size(414, 44);
            this.lblEntered.TabIndex = 21;
            this.lblEntered.Text = "Password";
            this.lblEntered.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.tlpRight.SetColumnSpan(this.lblData, 3);
            this.lblData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblData.Font = new System.Drawing.Font("Verdana", 14F);
            this.lblData.ForeColor = System.Drawing.Color.Thistle;
            this.lblData.Location = new System.Drawing.Point(7, 53);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(414, 97);
            this.lblData.TabIndex = 22;
            this.lblData.Text = "Settings data";
            this.lblData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCOL
            // 
            this.lblCOL.AutoSize = true;
            this.lblCOL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCOL.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCOL.ForeColor = System.Drawing.Color.Black;
            this.lblCOL.Location = new System.Drawing.Point(12, 0);
            this.lblCOL.Name = "lblCOL";
            this.lblCOL.Size = new System.Drawing.Size(92, 56);
            this.lblCOL.TabIndex = 6;
            this.lblCOL.Text = "COL";
            this.lblCOL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHeading.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.ForeColor = System.Drawing.Color.Black;
            this.lblHeading.Location = new System.Drawing.Point(110, 0);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(582, 56);
            this.lblHeading.TabIndex = 5;
            this.lblHeading.Text = "Manual Counter Workstation";
            this.lblHeading.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.Thistle;
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBack.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.ForeColor = System.Drawing.Color.Navy;
            this.btnBack.Location = new System.Drawing.Point(874, 514);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(92, 52);
            this.btnBack.TabIndex = 23;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblAuth
            // 
            this.lblAuth.AutoSize = true;
            this.tlpBorder.SetColumnSpan(this.lblAuth, 2);
            this.lblAuth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAuth.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Bold);
            this.lblAuth.ForeColor = System.Drawing.Color.Black;
            this.lblAuth.Location = new System.Drawing.Point(698, 0);
            this.lblAuth.Name = "lblAuth";
            this.lblAuth.Size = new System.Drawing.Size(268, 56);
            this.lblAuth.TabIndex = 24;
            this.lblAuth.Text = "Authorization Level: 0";
            this.lblAuth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(981, 569);
            this.Controls.Add(this.tlpBorder);
            this.Name = "FrmSetup";
            this.Text = "Form1";
            this.tlpBorder.ResumeLayout(false);
            this.tlpBorder.PerformLayout();
            this.tlpWhole.ResumeLayout(false);
            this.tlpLeft.ResumeLayout(false);
            this.tlpRight.ResumeLayout(false);
            this.tlpRight.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpBorder;
        private System.Windows.Forms.TableLayoutPanel tlpWhole;
        private System.Windows.Forms.TableLayoutPanel tlpLeft;
        private System.Windows.Forms.ListBox lbSettings;
        private System.Windows.Forms.TableLayoutPanel tlpRight;
        private System.Windows.Forms.Button btnBkSp;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btnChange;
        private System.Windows.Forms.Button btnDefault;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblEntered;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblCOL;
        private System.Windows.Forms.Label lblHeading;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblAuth;
    }
}


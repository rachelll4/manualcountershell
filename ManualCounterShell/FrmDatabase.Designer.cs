﻿namespace ManualCounterShell
{
    partial class FrmDatabase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpBorder = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblData = new System.Windows.Forms.Label();
            this.pbItemImage = new System.Windows.Forms.PictureBox();
            this.lbItems = new System.Windows.Forms.ListBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblCOL = new System.Windows.Forms.Label();
            this.lblHeading = new System.Windows.Forms.Label();
            this.tlpBorder.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemImage)).BeginInit();
            this.SuspendLayout();
            // 
            // tlpBorder
            // 
            this.tlpBorder.BackColor = System.Drawing.Color.LightSlateGray;
            this.tlpBorder.ColumnCount = 5;
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tlpBorder.Controls.Add(this.tableLayoutPanel1, 1, 1);
            this.tlpBorder.Controls.Add(this.btnBack, 3, 2);
            this.tlpBorder.Controls.Add(this.lblCOL, 1, 0);
            this.tlpBorder.Controls.Add(this.lblHeading, 2, 0);
            this.tlpBorder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpBorder.Location = new System.Drawing.Point(0, 0);
            this.tlpBorder.Name = "tlpBorder";
            this.tlpBorder.RowCount = 3;
            this.tlpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tlpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.Size = new System.Drawing.Size(1917, 1055);
            this.tlpBorder.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tlpBorder.SetColumnSpan(this.tableLayoutPanel1, 3);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63F));
            this.tableLayoutPanel1.Controls.Add(this.lblData, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.pbItemImage, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbItems, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(22, 108);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1871, 838);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblData.Font = new System.Drawing.Font("Verdana", 20F);
            this.lblData.ForeColor = System.Drawing.Color.Thistle;
            this.lblData.Location = new System.Drawing.Point(694, 419);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(1174, 419);
            this.lblData.TabIndex = 14;
            this.lblData.Text = "Touch item for info.";
            this.lblData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pbItemImage
            // 
            this.pbItemImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbItemImage.Location = new System.Drawing.Point(3, 422);
            this.pbItemImage.Name = "pbItemImage";
            this.pbItemImage.Size = new System.Drawing.Size(555, 413);
            this.pbItemImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbItemImage.TabIndex = 12;
            this.pbItemImage.TabStop = false;
            // 
            // lbItems
            // 
            this.lbItems.BackColor = System.Drawing.Color.Lavender;
            this.tableLayoutPanel1.SetColumnSpan(this.lbItems, 3);
            this.lbItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbItems.Font = new System.Drawing.Font("Verdana", 17F);
            this.lbItems.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lbItems.FormattingEnabled = true;
            this.lbItems.ItemHeight = 34;
            this.lbItems.Location = new System.Drawing.Point(3, 3);
            this.lbItems.Name = "lbItems";
            this.lbItems.Size = new System.Drawing.Size(1865, 413);
            this.lbItems.TabIndex = 13;
            this.lbItems.SelectedIndexChanged += new System.EventHandler(this.lbItems_SelectedIndexChanged);
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.Thistle;
            this.btnBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBack.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.ForeColor = System.Drawing.Color.Navy;
            this.btnBack.Location = new System.Drawing.Point(1708, 952);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(185, 100);
            this.btnBack.TabIndex = 8;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblCOL
            // 
            this.lblCOL.AutoSize = true;
            this.lblCOL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCOL.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCOL.ForeColor = System.Drawing.Color.Black;
            this.lblCOL.Location = new System.Drawing.Point(22, 0);
            this.lblCOL.Name = "lblCOL";
            this.lblCOL.Size = new System.Drawing.Size(185, 105);
            this.lblCOL.TabIndex = 6;
            this.lblCOL.Text = "COL";
            this.lblCOL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHeading.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.ForeColor = System.Drawing.Color.Black;
            this.lblHeading.Location = new System.Drawing.Point(213, 0);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(1489, 105);
            this.lblHeading.TabIndex = 5;
            this.lblHeading.Text = "Manual Counter Workstation";
            this.lblHeading.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmDatabase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(1917, 1055);
            this.Controls.Add(this.tlpBorder);
            this.Name = "FrmDatabase";
            this.Text = "FrmDatabase";
            this.tlpBorder.ResumeLayout(false);
            this.tlpBorder.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbItemImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpBorder;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.PictureBox pbItemImage;
        private System.Windows.Forms.ListBox lbItems;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblCOL;
        private System.Windows.Forms.Label lblHeading;
    }
}
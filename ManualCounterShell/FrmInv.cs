﻿//Author: Rachel Logan
//Date: 6/7/19

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using CoreScanner;
using ZeroMQ;
using System.Threading;

//IN ALL XML: USE SCANNER ID 1 FOR HANDHELD SCANNER; 2 FOR BOX SCANNER

namespace ManualCounterShell
{
    public partial class FrmInv : Form
    {
        static CCoreScanner cCoreScannerClass; //says use "CCoreScannerClass"
        Image imgCaptured;
        string imgfilename;
        string barInput;
        bool firstclick;
        bool issue;

        bool polling;
        int count;

        ZContext ctx;
        ZSocket receiver;
        ZSocket countListener;
        bool zmqOpen;
        Thread poller;
        StreamWriter sw;
        StreamWriter sw2;

        public FrmInv()
        {
            InitializeComponent();
            btnBack.Enabled = false;

            firstclick = true; //this is used for btnPhoto, init vs take
            issue = false; //this is for scanner init, see if scanner connected
            
            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;

            barInput = ""; //additive var straight from scanner
            count = 0; //pill count

            sw = new StreamWriter("output.txt"); //for general debugging

            long n = long.Parse(System.DateTime.Now.ToString("yyyyMMddHHmmss"));
            imgfilename = n.ToString() + ".jpg"; //one image title per count attempt: time of counting
            sw.WriteLine(imgfilename);

            polling = false; //this is var for infinite loop pausing in poll thread
            Thread init = new Thread(initialize); //takes care of all scanner/zmq inits
            init.Start();

            btnDone.Visible = false;
            btnPhoto.Visible = false;
            btnNewCnt.Visible = false;
            pbPill.Visible = false;
            btnSave.Visible = false;
            btnRetake.Visible = false;

        }

        #region initialization thread
        private void initialize() //this is on a thread
        {
            ZMQinit();

            //deal with start message: will either send "ready" or "not ready"
            string msg = receiver.ReceiveFrame().ReadString();
            if (msg != "ready")
            {
                lblStatus.Text = "Counter not initialized: " + msg;
            }
            sw.WriteLine("zmq complete");
            sw.Flush();

            polling = false;
            poller = new Thread(watchPython);
            poller.Start();
            //polling is false, tho

            if (!initCoreScan())
            {
                btnBack.BeginInvoke((MethodInvoker)delegate { btnBack.Enabled = true; });
                return; //if no scanner, don't continue
            }
            initScanner();
            initBarScan();

            btnBack.BeginInvoke((MethodInvoker)delegate { btnBack.Enabled = true; });
        }

        private void ZMQinit() 
        {
            ctx = new ZContext();
            receiver = new ZSocket(ctx, ZSocketType.PAIR);
            //bind socket
            receiver.Connect("tcp://127.0.0.1:5556");

            //this is the socket used for counting in the poller thread
            countListener = new ZSocket(ctx, ZSocketType.PAIR);
            countListener.Bind("tcp://127.0.0.1:5557");

            receiver.Send(new ZFrame("both connected"));
            sw.WriteLine("new poller socket: " + countListener.ReceiveFrame().ReadString());

            lblStatus.BeginInvoke((MethodInvoker)delegate { lblStatus.Text = "Connected to counter. Initializing counter..."; });
            zmqOpen = true;
        }

        //opens class, 
        //adds handlers, 
        //checks for a scanner: 
        //  if no, try again button+return; 
        //  if yes, calls initScanner
        private bool initCoreScan() 
        {
            lblStatus.BeginInvoke((MethodInvoker)delegate { lblStatus.Text = "Initializing scanner..."; });
            cCoreScannerClass = new CoreScanner.CCoreScanner();

            sw.WriteLine("trying Corescan open");
            sw.Flush();
            //Call Open API for scanner
            short[] scannerTypes = new short[1];
            scannerTypes[0] = 1; //for all scanner types
            short numOfScannerTypes = 1; //just size of array
            int status; //extended API return code
            cCoreScannerClass.Open(0, scannerTypes, numOfScannerTypes, out status);
            sw.WriteLine(status + " corescanner");
            sw.Flush();

            cCoreScannerClass.VideoEvent += new CoreScanner._ICoreScannerEvents_VideoEventEventHandler(OnVideoEvent);
            cCoreScannerClass.BarcodeEvent += new CoreScanner._ICoreScannerEvents_BarcodeEventEventHandler(OnBarcodeEvent);

            //check for connected scanners
            short numberOfScanners;
            int[] connectedScannerIDList = new int[255];
            string outXML;
            cCoreScannerClass.GetScanners(out numberOfScanners, connectedScannerIDList, out outXML, out status);
            sw.WriteLine("scanners: "+numberOfScanners); //write scanner ids to file
            sw.Flush();

            //if no scanners, trigger error
            //if no scanner
            if (numberOfScanners==Convert.ToInt16(0))
            {
                issue = true;
                lblStatus.BeginInvoke((MethodInvoker)delegate { lblStatus.Text = "Please connect a scanner."; });
                btnNewCnt.BeginInvoke((MethodInvoker)delegate { btnNewCnt.Text = "Try again"; btnNewCnt.Visible = true; });
                return false; //and break out all
            }

            //else, begin scanner init
            issue = false;
            return true;
        }

        //turns on scanning
        //switches to snapi image mode
        //registers for barcode and video events
        private void initScanner() {
            //turn on scanning 
            sw.WriteLine("scanner init");
            sw.Flush();
            int opcode = 2014; //enable scanning
            //string outXML;
            int scanonstatus;
            string outXML;
            string inXML = "<inArgs>" +
                         "<scannerID>1</scannerID>" +
                    "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out scanonstatus);
            sw.WriteLine(scanonstatus + " scan enabled");
            sw.Flush();

            //switch to snapi w imaging
            opcode = 6200; //opcode: switch host mode
            int SNAPIstatus;
            inXML = "<inArgs>" +
                        "<scannerID>1</scannerID>" +
                        "<cmdArgs>" +
                            "<arg-string>XUA-45001-9</arg-string>" + //to USB_SNAPI w/imaging
                            "<arg-bool>TRUE</arg-bool>" + //silent reboot
                            "<arg-bool>TRUE</arg-bool>" + //permanent
                        "</cmdArgs>" +
                    "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out SNAPIstatus);
            sw.WriteLine(SNAPIstatus + " snapi");
            sw.Flush();

            //register for video and barcodes
            opcode = 1001; //opcode: register for events
            int eventregstatus;
            inXML = "<inArgs>" +
                        "<cmdArgs>" +
                            "<arg-int>2</arg-int>" + //number of event types
                            "<arg-int>1,4</arg-int>" + // just image
                        "</cmdArgs>" +
                    "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out eventregstatus);
            sw.WriteLine(eventregstatus + " bar and vid events");
            sw.Flush();
        }

        //switches to barcode snapping mode
        private void initBarScan()
        {
            int opcode = 3500; //barcode mode
            int snapstatus;
            string outXML;
            string inXML = "<inArgs>" +
                                "<scannerID>1</scannerID>" +
                           "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out snapstatus);
            sw.WriteLine(snapstatus + " barmode");
            sw.Flush();
            lblStatus.BeginInvoke((MethodInvoker)delegate { lblStatus.Text = "Scan barcode to begin."; });
        }
        #endregion

        private void OnBarcodeEvent(short eventType, ref string scanData)
        {
            try
            {
                string tmpScanData = scanData;

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(tmpScanData);

                //use barInput
                string barcode = xmlDoc.DocumentElement.GetElementsByTagName("datalabel").Item(0).InnerText;
                string[] numbers = barcode.Split(' ');
                barInput = "";
                foreach (string number in numbers)
                {
                    if (String.IsNullOrEmpty(number))
                    {
                        break;
                    }
                    barInput += ((char)Convert.ToInt32(number, 16)).ToString();
                }
                sw.WriteLine("barcode received: " + barInput);
                sw.Flush();

                //turn off scanning bc obnoxious beeps
                int opcode = 2013; //disable scanning
                string outXML;
                int scanoffstatus;
                string inXML = "<inArgs>" +
                             "<scannerID>1</scannerID>" +
                        "</inArgs>";
                cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out scanoffstatus);
                sw.WriteLine(scanoffstatus + " scan disabled");
                sw.Flush();

                sw.WriteLine("restarting");
                receiver.SendFrame(new ZFrame("2")); //reset in case pills beforehand--don't count those (WAS 1)
                string msg = receiver.ReceiveFrame().ReadString();
                sw.WriteLine("restart received: " + msg);
                if (msg != "ready")
                {
                    sw.WriteLine("restart went wrong");
                    sw.Flush();
                    return;
                }
                sw.WriteLine("count reset");
                sw.Flush();

                polling = true;

                sw.WriteLine("polling started");
                sw.Flush();

                lblStatus.BeginInvoke((MethodInvoker)delegate () { lblStatus.Text = "Ready to count, or click Photo to preview photo."; });
                btnPhoto.BeginInvoke((MethodInvoker)delegate () { btnPhoto.Visible = true; });
                btnDone.BeginInvoke((MethodInvoker)delegate () { btnDone.Visible = true; });
            }
            catch (Exception e)
            {
                sw.WriteLine("error: " + e.Message);
            }
        }

        //thread action
        //while polling, listens for and writes count or error message
        private void watchPython()
        {
            sw2 = new StreamWriter("outputPoll.txt", true);
            sw2.WriteLine("poller started");
            sw2.Flush();
            //here, start poller for errors
            ZMessage pymsg;
            ZError zerror;

            var poll = ZPollItem.CreateReceiver();

            while (true)
            {
                if (polling) //polls recorded on different file
                {
                    sw2.Flush();
                    if (countListener.PollIn(poll, out pymsg, out zerror, TimeSpan.FromMilliseconds(20)))
                    {
                        string message = pymsg.PopString();
                        char index = message[0];
                        string communication = message.Substring(2);
                        sw2.WriteLine(communication);
                        if (index == '0') //then it's a count
                        {
                            count = Convert.ToInt32(communication);
                            lblCount.BeginInvoke((MethodInvoker)delegate { lblCount.Text = "Count: " + count; });
                        }
                        else
                            lblStatus.BeginInvoke((MethodInvoker)delegate { lblStatus.Text = message; });
                    }
                    else
                    {
                        if (zerror.Number != 11) sw2.WriteLine("Error: " + zerror.Text + " " + zerror.Number);
                        Thread.Sleep(60);
                    }
                    if (count > 0 && btnPhoto.Visible)
                        btnPhoto.Visible = false;
                }
                else
                {
                    lblCount.BeginInvoke((MethodInvoker)delegate { lblCount.Text = "Not counting"; });
                    Thread.Sleep(10);
                }
            }
        }

        private void btnNewCnt_Click(object sender, EventArgs e)
        {
            string outXML;
            if (issue)
            {
                //check for connected scanners
                short numberOfScanners;
                int[] connectedScannerIDList = new int[255];
                int status;
                cCoreScannerClass.GetScanners(out numberOfScanners, connectedScannerIDList, out outXML, out status);
                sw.WriteLine("scanners: "+numberOfScanners); //write scanner ids to file
                sw.Flush();

                //if no scanners, trigger error
                //if no scanner
                if (numberOfScanners == Convert.ToInt16(0))
                {
                    issue = true;
                    lblStatus.BeginInvoke((MethodInvoker)delegate { lblStatus.Text = "Please connect a scanner."; });
                    btnNewCnt.BeginInvoke((MethodInvoker)delegate { btnNewCnt.Text = "Try again"; btnNewCnt.Visible = true; });
                    return; //and break out all
                }

                //else, begin scanner init
                issue = false;
                btnNewCnt.BeginInvoke((MethodInvoker)delegate { btnNewCnt.Text = "New count"; btnNewCnt.Visible = false; });
                //and make sure try again button is hidden
                initScanner();
                initBarScan();

                polling = false;
                poller = new Thread(watchPython);
                poller.Start();
                //polling is false, tho
            }
            initBarScan();
            receiver.SendFrame(new ZFrame("2")); //reinit counter
            sw.WriteLine(receiver.ReceiveFrame().ReadString()); //ready signal
            sw.Flush();

            //turn on scanning 
            int opcode = 2014; //enable scanning
            int scanonstatus;
            string inXML = "<inArgs>" +
                         "<scannerID>1</scannerID>" +
                    "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out scanonstatus);
            sw.WriteLine(scanonstatus + " scan enabled");
            sw.Flush();

            lblStatus.Focus();
            //resets all
            barInput = "";
            imgCaptured = null;
            pbPill.Image = null;
            count = 0;

            long n = long.Parse(System.DateTime.Now.ToString("yyyyMMddHHmmss"));
            imgfilename = n.ToString() + ".jpg";
            
            btnDone.Visible = false;
            btnNewCnt.Visible = false;

            lblCount.Text = "Count: "+count;
            lblStatus.Text = "Scan barcode to begin.";
        }

        private void btnSave_Click(object sender, EventArgs e) //photo save
        {
            lblStatus.Focus();
            if (imgCaptured == null)
            {
                lblStatus.Text = "Take a photo.";
                return;
            }
            Save(imgCaptured); //calls helper function
            btnRetake.Visible = false;
            btnSave.Visible = false;

            receiver.SendFrame(new ZFrame("2")); //reset again (WAS 1)
            string msg = receiver.ReceiveFrame().ReadString();
            sw.WriteLine("restart received: " + msg);
            sw.Flush();
            if (msg != "ready")
            {
                sw.WriteLine("restart went wrong");
                sw.Flush();
                return;
            }
            polling = true;
            lblStatus.Text = "Ready to count.";
            btnDone.Visible = true;
        }

        private void btnRetake_Click(object sender, EventArgs e)
        {
            //turn on scanning
            int opcode = 2014; //enable scanning
            string outXML;
            int scanonstatus;
            string inXML = "<inArgs>" +
                         "<scannerID>1</scannerID>" +
                    "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out scanonstatus);
            sw.WriteLine(scanonstatus + " scan enabled");
            sw.Flush();

            lblStatus.Focus();
            pbPill.Image = null;
            imgCaptured = null;
            if (initVidScan()!=0)
                initVidScan();

            btnSave.Visible = false;
            btnRetake.Visible = false;
            btnPhoto.Visible = true;
            firstclick = false;

        }

        private void btnPhoto_Click(object sender, EventArgs e)
        {
            sw.WriteLine();
            if (firstclick)
            {
                polling = false;
                firstclick = false;
                lblStatus.Focus();

                //turns on video functionality, readies trigger click for image
                if (initVidScan() != 0)
                    initVidScan();
                btnPhoto.Text = "Take photo";
                lblStatus.Text = "Ready to take photo.";
                pbPill.Visible = true;
                btnPhoto.Visible = true;
                btnDone.Visible = false;
            }
            else{
                firstclick = true;
                btnSave.Visible = true;
                btnRetake.Visible = true;
                btnPhoto.Visible = false;
                takePhoto();

                //turn off scanning 
                int opcode = 2013; //disable scanning
                string outXML;
                int scanoffstatus;
                string inXML = "<inArgs>" +
                             "<scannerID>1</scannerID>" +
                        "</inArgs>";
                cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out scanoffstatus);
                sw.WriteLine(scanoffstatus + " photo: scan disabled");
                sw.Flush();
            }

        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            polling = false; //stop listening to counts

            //HERE, SAVE MOVIE--------------------------------------
            lblStatus.Text = "Saving video";
            lblStatus.Update();
            btnBack.Enabled = false;
            btnPhoto.Visible = false;
            btnDone.Visible = false;
            pbPill.Visible = false;

            receiver.SendFrame(new ZFrame("1")); //save current frames
            receiver.ReceiveFrame(); //this is 'done' or 'no vid to save'

            Inventory.Instance.add(barInput, count,imgfilename, DateTime.Now);
            lblStatus.Text = "Data saved. New count?";
            btnNewCnt.Visible = true; //only visible
            btnBack.Enabled = true;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            receiver.SendFrame(new ZFrame("3")); //tells python to close camera--says closing
            receiver.ReceiveFrame();
            if (poller!=null)
            {
                sw2.Close();
                poller.Abort();
            }
            if (zmqOpen)
            {
                countListener.Dispose();
                receiver.Dispose();
                ctx.Dispose();
            }
            sw.Close();

            //turn off scanning bc obnoxious beeps
            int opcode = 2013; //disable scanning
            string outXML;
            int scanoffstatus;
            string inXML = "<inArgs>" +
                         "<scannerID>1</scannerID>" +
                    "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out scanoffstatus);

            (new FrmStart()).Show();
            this.Hide();
        }

        //-------------------------------HANDLERS-------------------------------------------------
        void OnVideoEvent(short eventType, int size, ref object sfvideoData, ref string pScannerData)
        {
            sw.WriteLine("vid");
            try
            {
                Array arr = (Array)sfvideoData;
                long len = arr.LongLength;
                byte[] byImage = new byte[size];
                arr.CopyTo(byImage, 0);

                MemoryStream ms = new MemoryStream();
                ms.Write(byImage, 0, byImage.Length);

                Image img = Image.FromStream(ms);
                pbPill.Image = img;
                imgCaptured = img;
            }
            catch (Exception)
            {
                sw.WriteLine("no vid");
            }
        }

        //------------------------------------------------------------------------------
        private void Save(Image img)
        {
            //imgfilename is already determined for this bottle
            img.Save(imgfilename);
            lblStatus.Text = "Saved. Begin count or retake photo.";
        }

        private int initVidScan()
        {
            int total = 0;

            //turn on scanning 
            int opcode = 2014; //enable scanning
            string outXML;
            int scanonstatus;
            string inXML = "<inArgs>" +
                         "<scannerID>1</scannerID>" +
                    "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out scanonstatus);
            sw.WriteLine(scanonstatus + " photo: scan enabled");
            total += scanonstatus;
            sw.Flush();

            //image capture mode
            opcode = 4000; //video mode
            int snapstatus;
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out snapstatus);
            sw.WriteLine(snapstatus+" into video");
            total += snapstatus;
            sw.Flush();

            //also soft trigger
            opcode = 2011;
            int triggeronstatus;
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out triggeronstatus);
            sw.WriteLine(triggeronstatus + " triggered");
            total += triggeronstatus;
            sw.Flush();

            return total;
        }

        private void takePhoto()
        {
            //soft trigger release
            int opcode = 2012;
            string outXML;
            int triggeroffstatus;
            string inXML = "<inArgs>" +
                         "<scannerID>1</scannerID>" +
                    "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out triggeroffstatus);
            sw.WriteLine(triggeroffstatus + " untriggered");
            sw.Flush();
        }

        //-------------------------------------------------------------------------------------------------------
        #region extra xml functions
        //this basically updates all scanner attributes based on its readout when picture taken. 
        //not sure it's necessary

        public void ReadXmlString_FW(string strXml, out int nMax, out int nProgress, out string sStatus, out string csScannerID)
        {
            nMax = 0;
            nProgress = 0;
            sStatus = "";
            csScannerID = "";
            if (String.IsNullOrEmpty(strXml))
            {
                return;
            }

            string csSerial = "", csModel = "", csGuid = "";
            try
            {
                XmlTextReader xmlRead = new XmlTextReader(new StringReader(strXml));
                // Skip non-significant whitespace   
                xmlRead.WhitespaceHandling = WhitespaceHandling.Significant;

                string sElementName = "", sElmValue = "";
                bool bScanner = false;
                while (xmlRead.Read())
                {
                    switch (xmlRead.NodeType)
                    {
                        case XmlNodeType.Element:
                            sElementName = xmlRead.Name;
                            if ("scannerID" == sElementName) //from scanner constants
                            {
                                bScanner = true;
                            }
                            break;

                        case XmlNodeType.Text:
                            if (bScanner)
                            {
                                sElmValue = xmlRead.Value;
                                switch (sElementName)
                                {
                                    case "scannerID":
                                        csScannerID = sElmValue;
                                        break;
                                    case "serialnumber":
                                        csSerial = sElmValue;
                                        break;
                                    case "modelnumber":
                                        csModel = sElmValue;
                                        break;
                                    case "GUID":
                                        csGuid = sElmValue;
                                        break;
                                    case "status":
                                        sStatus = sElmValue;
                                        break;
                                    case "maxcount":
                                        nMax = Int32.Parse(sElmValue);
                                        break;
                                    case "progress":
                                        nProgress = Int32.Parse(sElmValue);
                                        break;
                                }
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        //this is just for this little thing in one of the imported helper methods
        private static string IndentXmlString(string strXml)
        {
            string outXml = string.Empty;
            MemoryStream ms = new MemoryStream();
            // Create a XMLTextWriter that will send its output to a memory stream (file)
            XmlTextWriter xtw = new XmlTextWriter(ms, Encoding.Unicode);
            XmlDocument doc = new XmlDocument();

            try
            {
                // Load the unformatted XML text string into an instance
                // of the XML Document Object Model (DOM)
                doc.LoadXml(strXml);

                // Set the formatting property of the XML Text Writer to indented
                // the text writer is where the indenting will be performed
                xtw.Formatting = Formatting.Indented;

                // write dom xml to the xmltextwriter
                doc.WriteContentTo(xtw);
                // Flush the contents of the text writer
                // to the memory stream, which is simply a memory file
                xtw.Flush();

                // set to start of the memory stream (file)
                ms.Seek(0, SeekOrigin.Begin);
                // create a reader to read the contents of
                // the memory stream (file)
                StreamReader sr = new StreamReader(ms);
                // return the formatted string to caller
                return sr.ReadToEnd();
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion
    }
}

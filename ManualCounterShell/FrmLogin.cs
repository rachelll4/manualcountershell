﻿//Author: Rachel Logan
//Date: 6/12/19

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DPUruNet;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Threading;
using System.IO;

namespace ManualCounterShell
{
    public partial class FrmLogin : Form
    {
        List<Fmd> preenrollmentFmds;
        Reader mReader;
        ReaderCollection allReaders;
        int count;
        int fingercount;
        string printssavefilename;
        string namessavefilename;
        List<Fmd> printDB;
        bool reconnect;
        List<string> names;
        string mName;
        bool typing;
        private const int DPFJ_PROBABILITY_ONE = 0x7fffffff;

        public FrmLogin()
        {
            InitializeComponent();
            preenrollmentFmds = new List<Fmd>();
            printssavefilename = "prints.txt";
            namessavefilename = "names.txt";

            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            reconnect = false;
            initScanner();
            printDB = LoadPrintDb();
        }

        private void btnNewUser_Click(object sender, EventArgs e)
        {
            if (typing)
            {
                typing = false;
                names.Add(mName); //adds name to end of list
                //should write this to file
                using (StreamWriter sw = File.AppendText(namessavefilename))
                {
                    sw.WriteLine(mName);
                }
                //also put everything to normal
                lblStatus.Text = "Enrollment complete. Scan fingerprint to access modules.";
                pbFingerprint.Visible = false;
                btnNewUser.Text = "New user";
                mName = null;
                this.KeyPress -= OnKeyPress; //unhook handler
                return;
            }

            if (reconnect)
            {
                reconnect = false;
                btnNewUser.Text = "New user";
                lblStatus.Text = "Scan fingerprint to access modules.";
                initScanner();
                printDB = LoadPrintDb();
                return;
            }

            count = 0;
            fingercount = 1;
            mReader.On_Captured -= OnCaptured;
            mReader.On_Captured += new Reader.CaptureCallback(OnEnrollCaptured);
            lblStatus.Text = "Scan finger you wish to enroll 4 times.";
            pbFingerprint.Visible = true;
            pbFingerprint.Image = null;
            btnNewUser.Visible = false;
        }

        //----------------------------------------------------------------------------
        #region Initialization stuff

        public bool initScanner()
        {
            //this is a collection of all connected scanners. since only one, take first index
            allReaders = ReaderCollection.GetReaders();
            if (allReaders.Count == 0)
            {
                Debug.WriteLine("No reader found.");
                lblStatus.BeginInvoke((MethodInvoker)delegate { lblStatus.Text = "Connect fingerprint scanner."; });
                btnNewUser.BeginInvoke((MethodInvoker)delegate { btnNewUser.Text = "Try again"; });
                reconnect = true;
                return false;
            }
            mReader = allReaders[0];
            if (mReader == null)
            {
                Debug.WriteLine("No reader found.");
                lblStatus.BeginInvoke((MethodInvoker)delegate { lblStatus.Text = "Connect fingerprint scanner."; });
                btnNewUser.BeginInvoke((MethodInvoker)delegate { btnNewUser.Text = "Try again"; });
                reconnect = true;
                return false;
            }

            Constants.ResultCode result = Constants.ResultCode.DP_DEVICE_FAILURE;
            result = mReader.Open(Constants.CapturePriority.DP_PRIORITY_COOPERATIVE);
            if(result != Constants.ResultCode.DP_SUCCESS)
            {
                Debug.WriteLine("Couldn't open reader: " + result);
                return false;
            }

            //now it's open, but still have to init for capture for identification
            //code below elims captureLoad and captureFingerAsync

            mReader.On_Captured += new Reader.CaptureCallback(OnCaptured); //adds handler **for identification
            try
            {
                FixStatus(); //throws Exs--keep this way
                Constants.ResultCode captureResult=mReader.CaptureAsync(Constants.Formats.Fid.ANSI, Constants.CaptureProcessing.DP_IMG_PROC_DEFAULT, mReader.Capabilities.Resolutions[0]);
                if (captureResult != Constants.ResultCode.DP_SUCCESS)
                {
                    Debug.WriteLine("Can't start capture: " + captureResult);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error: " + ex.Message);
                return false;
            }

            return true;
        }

        //will say whether capture worked--doesn't throw anything
        public bool CheckCaptureResult(CaptureResult captureResult)
        {
            if (captureResult.Data == null || captureResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
            {
                if (captureResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
                {
                    Debug.WriteLine("Capture unsuccessful: " + captureResult.ResultCode.ToString());
                }

                // Send message if quality shows fake finger
                if ((captureResult.Quality != Constants.CaptureQuality.DP_QUALITY_CANCELED))
                {
                    Debug.WriteLine("Quality issue: " + captureResult.Quality);
                }
                return false;
            }
            return true;
        }

        //this is good stuff --throws exceptions
        public void FixStatus()
        {
            Constants.ResultCode result = mReader.GetStatus();

            if ((result != Constants.ResultCode.DP_SUCCESS))
            {
                throw new Exception("" + result);
            }

            if ((mReader.Status.Status == Constants.ReaderStatuses.DP_STATUS_BUSY))
            {
                Thread.Sleep(50);
            }
            else if ((mReader.Status.Status == Constants.ReaderStatuses.DP_STATUS_NEED_CALIBRATION))
            {
                mReader.Calibrate();
            }
            else if ((mReader.Status.Status != Constants.ReaderStatuses.DP_STATUS_READY))
            {
                throw new Exception("Reader Status - " + mReader.Status.Status);
            }
        }

        //used when data comes in, making legible to methods
        public Bitmap CreateBitmap(byte[] bytes, int width, int height)
        {
            byte[] rgbBytes = new byte[bytes.Length * 3];

            for (int i = 0; i <= bytes.Length - 1; i++)
            {
                rgbBytes[(i * 3)] = bytes[i];
                rgbBytes[(i * 3) + 1] = bytes[i];
                rgbBytes[(i * 3) + 2] = bytes[i];
            }
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            BitmapData data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);

            for (int i = 0; i <= bmp.Height - 1; i++)
            {
                IntPtr p = new IntPtr(data.Scan0.ToInt64() + data.Stride * i);
                System.Runtime.InteropServices.Marshal.Copy(rgbBytes, i * bmp.Width * 3, p, bmp.Width * 3);
            }

            bmp.UnlockBits(data);

            return bmp;
        }


        #endregion
        //----------------------------------------------------------------------------
        #region Enrollment methodry

        private void OnEnrollCaptured(CaptureResult captureResult)
        {
            try
            {
                // Check capture quality and throw an error if bad.
                if (!CheckCaptureResult(captureResult)) return;

                //display it
                foreach (Fid.Fiv fiv in captureResult.Data.Views)
                {
                    pbFingerprint.BeginInvoke(
                        (MethodInvoker)delegate {
                            pbFingerprint.Image = CreateBitmap(fiv.RawImage, fiv.Width, fiv.Height);
                        }
                    );
                }
                count++;
                
                DataResult<Fmd> resultConversion = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.ANSI);

                //see if already enrolled
                int thresholdScore = DPFJ_PROBABILITY_ONE * 1 / 100000;
                IdentifyResult identifyResult = Comparison.Identify(resultConversion.Data, 0, printDB, thresholdScore, 1);
                if (identifyResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
                {
                    Debug.WriteLine("Identification failed: " + identifyResult.ResultCode.ToString());
                }
                else
                {
                    int matches = identifyResult.Indexes.Length;
                    if (matches > 0)
                    {
                        lblStatus.BeginInvoke(
                            (MethodInvoker)delegate
                            {
                                lblStatus.Text ="You are already enrolled in this system. Please scan to access modules.";
                            }
                        );
                        mReader.On_Captured -= OnEnrollCaptured; //unhooks handler
                        mReader.On_Captured += new Reader.CaptureCallback(OnCaptured);
                        btnNewUser.BeginInvoke((MethodInvoker)delegate { btnNewUser.Visible = true; });
                        return;
                    }
                }


                lblStatus.BeginInvoke((MethodInvoker)delegate () { lblStatus.Text = "Finger "+fingercount+" captured x"+count; });
                if (resultConversion.ResultCode != Constants.ResultCode.DP_SUCCESS)
                {
                    Debug.WriteLine("Conversion unsuccessful: "+resultConversion.ResultCode.ToString());
                }
                preenrollmentFmds.Add(resultConversion.Data);

                if (count >= 4) //if 4 captures have been successful for one finger
                {
                    DataResult<Fmd> resultEnrollment = DPUruNet.Enrollment.CreateEnrollmentFmd(Constants.Formats.Fmd.ANSI, preenrollmentFmds);

                    if (resultEnrollment.ResultCode == Constants.ResultCode.DP_SUCCESS)
                    {
                        lblStatus.BeginInvoke((MethodInvoker)delegate () { lblStatus.Text = "Finger enrolled. Scan next finger 4 times."; });
                        preenrollmentFmds.Clear(); //start clean for new finger
                        fingercount++;
                        count = 0;

                        //save to file
                        try
                        {
                            using (StreamWriter sw = File.AppendText(printssavefilename))
                            {
                                sw.WriteLine(Fmd.SerializeXml(resultEnrollment.Data));
                            }
                        }
                        catch { }

                        //if done enrolling, unhook handler and change labels
                        if (fingercount > 4)
                        {
                            mReader.On_Captured -= OnEnrollCaptured; //unhooks handler
                            lblStatus.BeginInvoke((MethodInvoker)delegate () { lblStatus.Text = "Enrollment complete. Enter your name."; });

                            //hooks in a key handler which interacts with lblStatus and a var
                            this.KeyPreview = true; //for counting 
                            this.KeyPress += new KeyPressEventHandler(OnKeyPress);
                            mName = null;
                            typing = true;
                            //right here, new user becomes a 'done' button which completes name entry
                            btnNewUser.BeginInvoke((MethodInvoker)delegate { btnNewUser.Text = "Done"; });

                            mReader.On_Captured += new Reader.CaptureCallback(OnCaptured);
                            printDB = LoadPrintDb();
                        }

                        return;
                    }
                    else if (resultEnrollment.ResultCode == Constants.ResultCode.DP_ENROLLMENT_INVALID_SET)
                    {
                        lblStatus.BeginInvoke((MethodInvoker)delegate () { lblStatus.Text = "Finger enrollment failed. Enroll finger "+fingercount+" again."; });
                        preenrollmentFmds.Clear();
                        count = 0;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error: " + ex.Message);
            }
        }

        private void OnKeyPress(object sender, KeyPressEventArgs e) //for demo: counts space press as pill
        {
            if (e.KeyChar>31) //leaves range of all typed chars, including bksp, maybe
            {
                mName += e.KeyChar;
                lblStatus.Text = "Name: "+mName;
            }
            lblStatus.BeginInvoke((MethodInvoker)delegate { lblStatus.Focus(); });
            btnNewUser.BeginInvoke((MethodInvoker)delegate { btnNewUser.Visible = true; });
        }

        #endregion
        //----------------------------------------------------------------------------
        #region Identification things

        //handler for identification: default handler
        public void OnCaptured(CaptureResult captureResult)
        {
            // Check capture quality and throw an error if bad.
            if (!CheckCaptureResult(captureResult)) return;

            //makes an fmd to check
            DataResult<Fmd> resultConversion = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.ANSI);

            int thresholdScore = DPFJ_PROBABILITY_ONE * 1 / 100000;

            //here, instead of passing a whole database, pass in by for loop
            //index divide by 4 is index of name in file
            List<Fmd> singular = new List<Fmd>();
            for (int c = 0; c < printDB.Count; c++) //zero indexed
            {
                singular.Clear();
                singular.Add(printDB.ElementAt(c)); //holding just this one fmd
                IdentifyResult identifyResult = Comparison.Identify(resultConversion.Data, 0, singular, thresholdScore, 1);
                if (identifyResult.ResultCode != Constants.ResultCode.DP_SUCCESS)
                {
                    Debug.WriteLine("Identification failed: " + identifyResult.ResultCode.ToString());
                    return;
                }
                int matches = identifyResult.Indexes.Length;
                if (matches > 0)
                {
                    //here, grab name
                    int nameindex = c / 4; //truncate is fine bc 3/4<1 bc it's the first name in nameDB at index 0
                    //how pass to next screen? save in inventory? why not?
                    Inventory.Instance.nameOfCurrentPharmacist = names.ElementAt(nameindex);

                    this.BeginInvoke((MethodInvoker)delegate
                    {
                        (new FrmStart()).Show();
                        this.Hide();
                    });
                    return;
                }
            }
            //if got to this point without returning and switching form, must not be present fingerprint
            lblStatus.BeginInvoke((MethodInvoker)delegate
            {
                lblStatus.Text = "Fingerprint not recognized. Try again, or try a different finger.";
            });
        }

        public List<Fmd> LoadPrintDb()
        {
            List<Fmd> FmdDB = new List<Fmd>();
            names = new List<string>();
            string tempLine;
            if (!File.Exists(printssavefilename)) //prints
            {
                File.Create(printssavefilename);
            }
            if (!File.Exists(namessavefilename)) //names
            {
                File.Create(namessavefilename);
            }
            using (StreamReader sr=new StreamReader(printssavefilename)) //prints
            {
                //for each line, read in as xml and deserialize, add that
                while (!sr.EndOfStream)
                {
                    tempLine = sr.ReadLine();
                    FmdDB.Add(Fmd.DeserializeXml(tempLine));
                }

            }
            using (StreamReader sr = new StreamReader(namessavefilename)) //names
            {
                //for each line, read in as xml and deserialize, add that
                while (!sr.EndOfStream)
                {
                    names.Add(sr.ReadLine()); //each line in file is a name associated with the print at that index
                }

            }
            return FmdDB;
        }


        #endregion
    }
}

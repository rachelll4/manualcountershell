﻿//Author: Rachel Logan
//Date: 5/22/19

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManualCounterShell
{
    public partial class FrmStart : Form
    {
        public FrmStart()
        {
            InitializeComponent();
            lblWelcome.Text = "Welcome, " + Inventory.Instance.nameOfCurrentPharmacist + "! Select mode.";
            if (!Inventory.Instance.getBarcodeScanIsOn())
            {
                btnMdInv.Enabled = false;
                btnMdInv.BackColor = Color.DarkSlateGray;
                lblWelcome.Text += "\nBarcode scanner disabled; Inventory mode unavailable.";
            }
            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
        }

        private void btnMdCount_Click(object sender, EventArgs e)
        {
            (new FrmCount()).Show();
            this.Hide();
        }

        private void btnMdInv_Click(object sender, EventArgs e)
        {
            (new FrmInv()).Show();
            this.Hide();
        }

        private void btnSetup_Click(object sender, EventArgs e)
        {
            (new FrmSetup()).Show();
            this.Hide();
        }

        private void btnDb_Click(object sender, EventArgs e)
        {
            (new FrmDatabase()).Show();
            this.Hide();
        }

        private void btnOut_Click(object sender, EventArgs e)
        {
            (new FrmLogin()).Show();
            this.Hide();
        }
    }
}

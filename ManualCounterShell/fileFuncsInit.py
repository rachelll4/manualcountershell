# -*- coding: utf-8 -*-
"""
@author Rachel Logan
June 28 2019
"""
#%%
import zmq

#set up ZMQ socket etc
context=zmq.Context()
socket=context.socket(zmq.PAIR)
socket.connect("tcp://127.0.0.1:5558")   #connects

#having it send one message back after initialization
print(socket.recv())
socket.send_string("received")

key='a8cc75d'

while True:
    comm=str(socket.recv_string())
    index=comm[0]
            
    if index=='0': #encode
        with open("paramData.txt","r") as f:
            string=f.read()

        encoded_chars = []
        for i in range(len(string)):
            key_c = key[i % len(key)]
            encoded_c = chr(ord(string[i]) + ord(key_c) % 256)
            encoded_chars.append(encoded_c)
        encoded_string = "".join(encoded_chars)

        with open("secureFile.txt","w",encoding='utf-8-sig') as f:
            f.write(encoded_string)
        socket.send_string("0")

    elif index=='1': #decode
        with open("secureFile.txt","r",encoding='utf-8-sig') as f:
            string=f.read()

        encoded_chars = []
        for i in range(len(string)):
            key_c = key[i % len(key)]
            encoded_c = chr(ord(string[i]) - ord(key_c) % 256)
            encoded_chars.append(encoded_c)
        decoded_string = "".join(encoded_chars)
        
        with open("paramData.txt","w") as f:
            f.write(decoded_string)     
        socket.send_string("0")


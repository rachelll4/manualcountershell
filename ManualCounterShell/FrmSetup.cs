﻿//Author: Rachel Logan
//Date: 5/23/19

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZeroMQ;

namespace ManualCounterShell
{
    public partial class FrmSetup : Form
    {
        const string VERSION = "19.09.23.1";

        string keyEntry;
        string keyEntered;
        int authLevel;
        string paramListFilename;
        string paramDataFilename;
        int numOfParamsExpected;
        string[][] loadedParams;
        string[][] loadedData;
        int numDataParamsExp;
        string auth1pass;
        string auth2pass;
        int listBoxIndex;
        int keyAuthFileIndex;
        int paramFileIndex;
        bool action;
        bool stars;

        ZContext ctx;
        ZSocket receiver;
        Process proc; //this is the python file running in the background, to close later
        bool zmqOpen;

        public FrmSetup()
        {
            InitializeComponent();
            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;

            keyEntry = "";
            keyEntered = "";

            auth1pass = "1111"; //ENCRYPT LATER
            auth2pass = "2222";

            authLevel = 0; //0 is base, 1 is technician, and 2 is COL ppl
            paramListFilename = "paramTry.txt";
            paramDataFilename = "paramData.txt";
            numOfParamsExpected = 28; //update this if you add any options
            numDataParamsExp = 20;
            loadedParams = new string[numOfParamsExpected][]; //the 6 is how many fields
            loadedData = new string[numDataParamsExp][]; //3 is num fields
            action = false;

            ZMQinit();
            loadParams();

            authLevel = Inventory.Instance.checkStillLoggedInAuth();
            populateList(authLevel);

            lbSettings.SelectedIndexChanged += lbSettings_OnSelectedIndexChanged;

            btnChange.Visible = false;
            btnChange.Hide();
            btnCancel.Visible = false;
            btnCancel.Hide();
            btnDefault.Visible = false;
            btnDefault.Hide();
            keypadEnabledIs(false);
        }
        //-------------------------------------------------------------
        #region settings buttons

        private void keypadEnabledIs(bool clickability)
        {
            btnGo.Visible = clickability;
            btn1.Enabled = clickability;
            btn2.Enabled = clickability;
            btn3.Enabled = clickability;
            btn4.Enabled = clickability;
            btn5.Enabled = clickability;
            btn6.Enabled = clickability;
            btn7.Enabled = clickability;
            btn8.Enabled = clickability;
            btn9.Enabled = clickability;
            btn0.Enabled = clickability;
            btnBkSp.Enabled = clickability;

            btn0.BackColor = (clickability) ? Color.DarkSeaGreen : Color.Thistle;
            btn1.BackColor = (clickability) ? Color.DarkSeaGreen : Color.Thistle;
            btn2.BackColor = (clickability) ? Color.DarkSeaGreen : Color.Thistle;
            btn3.BackColor = (clickability) ? Color.DarkSeaGreen : Color.Thistle;
            btn4.BackColor = (clickability) ? Color.DarkSeaGreen : Color.Thistle;
            btn5.BackColor = (clickability) ? Color.DarkSeaGreen : Color.Thistle;
            btn6.BackColor = (clickability) ? Color.DarkSeaGreen : Color.Thistle;
            btn7.BackColor = (clickability) ? Color.DarkSeaGreen : Color.Thistle;
            btn8.BackColor = (clickability) ? Color.DarkSeaGreen : Color.Thistle;
            btn9.BackColor = (clickability) ? Color.DarkSeaGreen : Color.Thistle;
        }
        private void btnGo_Click(object sender, EventArgs e)
        {
            keyEntered = String.Copy(keyEntry);
            keyEntry = "";
            lblEntered.Text = "";
            
            if (loadedParams[keyAuthFileIndex][1] == "enterPassword") { 
                if (String.Compare(keyEntered, auth1pass) == 0)
                {
                    lblEntered.Text = "Logged in. ";
                    authLevel = 1;
                    Inventory.Instance.login(1);
                    lblAuth.Text = "Authorization Level " + authLevel;
                    populateList(1);
                    keypadEnabledIs(false);
                }
                else if (String.Compare(keyEntered, auth2pass) == 0)
                {
                    lblEntered.Text = "Logged in. ";
                    authLevel = 2;
                    Inventory.Instance.login(2);
                    lblAuth.Text = "Authorization Level " + authLevel;
                    populateList(2);
                    keypadEnabledIs(false);
                }
                else
                {
                    lblEntered.Text = "Incorrect password. ";
                    authLevel = 0;
                    Inventory.Instance.login(0);
                    lblAuth.Text = "Authorization Level " + authLevel;
                    populateList(0);
                }
            }
            else //any param value
            {
                loadedData[paramFileIndex][1] = keyEntered;
                //save to file
                lblData.Text = "Current value updated: " + loadedData[paramFileIndex][1] + ". ";
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            reset();
        }

        private void reset()
        {
            btnChange.Visible = false;
            btnCancel.Visible = false;
            btnDefault.Visible = false;
            btnDefault.Text = "Default";
            keypadEnabledIs(false);
            keyEntered = "";
            keyEntry = "";
            lblEntered.Text = "";
            //lblData.Text += "\n";
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            //prereq: auth level to change met
            keypadEnabledIs(true);
        }

        private void btnDefault_Click(object sender, EventArgs e)
        {
            if (action) //acting as confirm button
            {
                //check which action to be executed
                if (loadedParams[keyAuthFileIndex][1] == "exitMainApp")//exit application
                {
                    if (zmqOpen)
                    {
                        if (!proc.HasExited || proc != null) { proc.Kill(); }
                        receiver.Dispose();
                        ctx.Dispose();
                    }
                    ZContext temp = new ZContext();
                    ZSocket socket = new ZSocket(temp, ZSocketType.PAIR);
                    socket.Connect("tcp://127.0.0.1:5556");
                    socket.SendFrame(new ZFrame("save"));
                    socket.ReceiveFrame(); //should be "done"
                    socket.Send(new ZFrame("close"));
                    socket.ReceiveFrame(); //should be "closing"
                    socket.Dispose();
                    temp.Dispose();
                    Application.Exit();
                }
                else if (loadedParams[keyAuthFileIndex][1] == "clearDatabase") //clear prescription inventory database
                {
                    Inventory.Instance.clearDatabase(); //also deletes the savefile
                }
                else if (loadedParams[keyAuthFileIndex][1] == "setAllComputerCurrentValues2Default") //all current values are written in as defaults
                {
                    //go through every parameter value and change the default value to equal current value
                    for (int c = 0; c < numDataParamsExp; c++) //iterates through just file of parameters, not actions
                    {
                        loadedData[paramFileIndex][2] = loadedData[paramFileIndex][1]; //default=current
                    }
                    lblData.Text = "Default values updated.";
                    saveParams();
                    btnCancel.PerformClick();
                }
                else if (loadedParams[keyAuthFileIndex][1] == "setAllComputerDefaultValues") //all current values go back to default values 
                {
                    //go through every parameter value and change the current value to equal default value
                    for (int c = 0; c < numDataParamsExp; c++) //iterates through just file of parameters, not actions
                    {
                        loadedData[paramFileIndex][1] = loadedData[paramFileIndex][2]; //default=current
                    }
                    lblData.Text = "Current values updated.";
                    saveParams();
                    btnCancel.PerformClick();
                }
                else if (loadedParams[keyAuthFileIndex][1] == "viewVideoList" 
                    || loadedParams[keyAuthFileIndex][1] == "viewCameraFeedStart") //view all or view feed
                {
                    saveParams();
                    if (loadedParams[keyAuthFileIndex][1] == "viewCameraFeedStart")
                    {
                        Inventory.Instance.setFeedView(true);
                    }
                    if (zmqOpen)
                    {
                        if (!proc.HasExited || proc != null) { proc.Kill(); }
                        receiver.Dispose();
                        ctx.Dispose();
                    }
                    (new FrmVideo()).Show();
                    this.Hide();
                }
                //for next two, acting as switch button
                else if (loadedParams[keyAuthFileIndex][1] == "toggleBarcodeScanner") //toggle barcode
                {
                    //check for current value and make it the other
                    //also set an Inventory-level variable to check on startup
                    lblData.Text = "Barcode scanning is ";
                    if (loadedData[0][1] == "0")
                    {
                        loadedData[0][1] = "1";
                        lblData.Text += "on.";
                    }
                    else if (loadedData[0][1] == "1")
                    {
                        loadedData[0][1] = "0";
                        lblData.Text += "off.";
                    }
                    Inventory.Instance.toggleBarcodeScan();
                    
                }
                else if (loadedParams[keyAuthFileIndex][1] == "toggleFingerprintScanner") //toggle fingerprint
                {
                    lblData.Text = "Fingerprint scanning is ";
                    if (loadedData[1][1] == "0")
                    {
                        loadedData[1][1] = "1";
                        lblData.Text += "on.";
                    }
                    else if (loadedData[1][1] == "1")
                    {
                        loadedData[1][1] = "0";
                        lblData.Text += "off.";
                    }
                    Inventory.Instance.toggleFingerScan();
                }
                else if (loadedParams[keyAuthFileIndex][1] == "flipImage") //toggle fingerprint
                {
                    lblData.Text = "Image flipping is ";
                    if (loadedData[2][1] == "0")
                    {
                        loadedData[2][1] = "1";
                        lblData.Text += "on.";
                    }
                    else if (loadedData[2][1] == "1")
                    {
                        loadedData[2][1] = "0";
                        lblData.Text += "off.";
                    }
                    Inventory.Instance.toggleFlipImage();
                }
                //also reset and hide again
                btnDefault.Visible = false;
                btnDefault.Text = "Default";
                action = false;
                return;
            }
            //not confirm, literally just setting current value back to default
            loadedData[paramFileIndex][1] = loadedData[paramFileIndex][2]; //current=default
            lblData.Text = "Current value updated: " + loadedData[paramFileIndex][1] + ". ";
            btnCancel.PerformClick();
        }
        #endregion
        //-----------------------------------------------------------------------
        #region param/listbox
        private void lbSettings_OnSelectedIndexChanged(object sender, System.EventArgs e)
        {
            stars = false;
            if (lbSettings.SelectedIndex < 0) return; //if they didn't select one, hit blank space
            lblData.Text = "";
            reset();

            //-----------------check login status----------------
            if (Inventory.Instance.checkStillLoggedInAuth() != authLevel) //logged out
            {
                lblData.Text = "Ten minutes since last acivity: logged out.";
                authLevel = Inventory.Instance.checkStillLoggedInAuth();
                populateList(authLevel);
                return;
            }
            Inventory.Instance.login(authLevel);
            lblAuth.Text = "Authorization Level " + authLevel;
            //-------------------------------------------------------

            listBoxIndex = lbSettings.SelectedIndex;
            //need loop to determine index
            int count = -1;
            for (keyAuthFileIndex=0; keyAuthFileIndex<numOfParamsExpected; keyAuthFileIndex++)
            {
                if (Convert.ToInt32(loadedParams[keyAuthFileIndex][4]) <= authLevel)
                {
                    count++; //now at zero, holding zero index
                }
                if (count == listBoxIndex)
                {
                    break;
                }
            } //finds which is selected
            lblData.Text += loadedParams[keyAuthFileIndex][2]; //text name
            if (loadedParams[keyAuthFileIndex][0] == "z") //if it's an action: 0 is index of z/p, action or param, decides whether getting other data or not
            {
                //these are the gui level options
                if ((loadedParams[keyAuthFileIndex][1] == "exitMainApp" //exit application (skip 3 bc that's show version number)
                    || loadedParams[keyAuthFileIndex][1] == "clearDatabase" //clear database
                    || loadedParams[keyAuthFileIndex][1] == "setAllComputerCurrentValues2Default" //set current values are now the default values
                    || loadedParams[keyAuthFileIndex][1] == "setAllComputerDefaultValues"//set vals back to default
                    || loadedParams[keyAuthFileIndex][1] == "viewVideoList" //view recording list
                    || loadedParams[keyAuthFileIndex][1] == "viewCameraFeedStart") //view camera feed
                    && Convert.ToInt32(loadedParams[keyAuthFileIndex][5]) <= authLevel) //AND it's allowed
                {
                    //show btnDefault as a confirm button
                    btnDefault.Text = "Confirm";
                    btnDefault.Visible = true;
                    action = true;
                }
                else if(loadedParams[keyAuthFileIndex][1] == "showSoftwareVersion") //show version number
                {
                    lblData.Text += "\n" + VERSION;
                }

                else if ((loadedParams[keyAuthFileIndex][1] == "toggleBarcodeScanner")//if toggle barcode scanners
                    && Convert.ToInt32(loadedParams[keyAuthFileIndex][5]) <= authLevel) //and allowed
                {
                    //these are currently the first and second in data file
                    //so lblData.Text+= on/off based on
                    //so ask what the current value is in paramData
                    //then add to lblData based on that
                    //then show btnDefault as "Switch"
                    //have to separate bc diff indices
                    if (loadedData[0][1] == "0") //if off: first index means first spot in paramData
                        lblData.Text += " is off";
                    else if (loadedData[0][1] == "1") //is on
                        lblData.Text += " is on";

                    btnDefault.Text = "Switch";
                    btnDefault.Visible = true;
                    action = true;
                }

                else if ((loadedParams[keyAuthFileIndex][1] == "toggleFingerprintScanner")//if toggle finger scanners
                    && Convert.ToInt32(loadedParams[keyAuthFileIndex][5]) <= authLevel) //and allowed
                {
                    if (loadedData[1][1] == "0") //if off: first index means second spot in paramData
                        lblData.Text += " is off";
                    else if (loadedData[1][1] == "1") //is on
                        lblData.Text += " is on";

                    btnDefault.Text = "Switch";
                    btnDefault.Visible = true;
                    action = true;
                }
                else if ((loadedParams[keyAuthFileIndex][1] == "flipImage")//if toggle finger scanners
                    && Convert.ToInt32(loadedParams[keyAuthFileIndex][5]) <= authLevel) //and allowed
                {
                    if (loadedData[2][1] == "0") //if off: first index means second spot in paramData
                        lblData.Text += " is off";
                    else if (loadedData[2][1] == "1") //is on
                        lblData.Text += " is on";

                    btnDefault.Text = "Switch";
                    btnDefault.Visible = true;
                    action = true;
                }

                else if (loadedParams[keyAuthFileIndex][1] == "enterPassword") //enter password //can't do it off index bc changes based on current auth level
                {
                    stars = true;
                    keypadEnabledIs(true);
                }
            }
            else //it's a parameter
            {
                //JUST MATCH BY FIELD 1--this is the key, first field in other doc
                string key = loadedParams[keyAuthFileIndex][1]; //1 is field of var name
                paramFileIndex = 0;
                while(paramFileIndex < numDataParamsExp){
                    if (String.Compare(loadedData[paramFileIndex][0], key) == 0)
                    {
                        break;
                    }
                    paramFileIndex++;
                }
                //holding correct param data index
                lblData.Text += "\nCurrent: " + loadedData[paramFileIndex][1] + ". Default: " + loadedData[paramFileIndex][2] + ".";
                //also, if able, enable change/default/cancel
                if (Convert.ToInt32(loadedParams[keyAuthFileIndex][5]) <= authLevel)
                {
                    btnChange.Visible = true;
                    btnCancel.Visible = true;
                    btnDefault.Visible = true;
                }
            }
        }

        private void populateList(int authLevel)
        {
            //all levels populate level 0--cascading if statements
            //level 0 doesn't even need a case
            //level one settings
            lbSettings.Items.Clear();
            for (int x=0; x<numOfParamsExpected; x++) //only loop through once
            {
                //hold each param, ask it what auth can see it
                if (loadedParams[x][4] == "0") //4 is field with visibility--but it's a string
                {
                    lbSettings.Items.Add(loadedParams[x][2]); //2 is the field with the title to show
                }
                
                if (authLevel >= 1)
                {
                    if (loadedParams[x][4] == "1") //4 is field with visibility--but it's a string
                    {
                        lbSettings.Items.Add(loadedParams[x][2]); //2 is the field with the title to show
                    }
                }
                if(authLevel == 2)
                {
                    if (loadedParams[x][4] == "2") //4 is field with visibility--but it's a string
                    {
                        lbSettings.Items.Add(loadedParams[x][2]); //2 is the field with the title to show
                    }
                }
            }
        }

        private void loadParams()
        {
            //read from text file into arrays
            //read in by lines, then char by char
            //need string tempLine, why need varname?
            //do i need a scanner?
            string tempLine;
            int indexParam=0;
            try
            {
                using (StreamReader sr = new StreamReader(paramListFilename))
                {
                    //for every line--while lines?
                    while (!sr.EndOfStream) //wish i could use hasNext...thx java //could use for loop with numOfParamsExpected
                    {
                        tempLine = sr.ReadLine();
                        //parse the line for format, sep by ','
                        loadedParams[indexParam] = tempLine.Split(',');
                        indexParam++; //when no more chars in line, next param
                    }
                }
            }
            catch (IOException e)
            {
                Debug.WriteLine("File couldn't be opened: " + e.Message);
            }

            //now load default/current param data
            indexParam = 0;
            try
            {
                //decodeFromFile();
                using (StreamReader sr = new StreamReader(paramDataFilename))
                {
                    while (!sr.EndOfStream)
                    {
                        tempLine = sr.ReadLine();
                        loadedData[indexParam] = tempLine.Split(',');
                        indexParam++; //when no more chars in line, next param
                    }
                }
            }
            catch (IOException e2)
            {
                Debug.WriteLine("File not opened: " + e2.Message);
            }
        }

        private void saveParams()
        {
            //every time the auth changes and on close and exit
            //only need to save to data file 
            using(StreamWriter sw=new StreamWriter(paramDataFilename))
            {
                for (int i=0; i < numDataParamsExp; i++){ //for every param
                    sw.WriteLine(loadedData[i][0] + "," + loadedData[i][1] + "," + loadedData[i][2]);
                }
            }
            encodeToFile();
            Inventory.Instance.readInParams();

            ZContext temp = new ZContext();
            ZSocket socket = new ZSocket(temp, ZSocketType.PAIR);
            socket.Connect("tcp://127.0.0.1:5556");
            socket.Send(new ZFrame("save"));
            socket.ReceiveFrame(); //should be "done"
            socket.Dispose();
            temp.Dispose();
        }

        private void encodeToFile()
        {
            Debug.WriteLine("encoding");
            receiver.SendFrame(new ZFrame("0"));
            Debug.WriteLine("saved: "+receiver.ReceiveFrame());
        }

        #endregion

        #region zmq+python
        private void ZMQinit()
        {
            ctx = new ZContext();
            receiver = new ZSocket(ctx, ZSocketType.PAIR);

            //bind socket
            receiver.Bind("tcp://127.0.0.1:5558"); //same "address" as in videoGuiFuncs
            Debug.WriteLine("bound.");

            runPythonVidScript();

            //send to running py file and receive back, to check
            receiver.SendFrame(new ZFrame("sending"));
            Debug.WriteLine(receiver.ReceiveFrame().ReadString());
            //should write "received"
            zmqOpen = true;
        }

        private void runPythonVidScript()
        {
            try
            {
                string fm = Path.GetFullPath("fileFuncsInit.pyc");
                var info = new ProcessStartInfo("python", "\""+fm+"\"");
                //this filename can be changed to get root and add to that

                info.RedirectStandardInput = false;
                info.RedirectStandardOutput = true;
                info.UseShellExecute = false;
                info.CreateNoWindow = true;

                proc = new Process();
                proc.StartInfo = info;
                proc.Start();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Script failed: " + ex.Message);
            }
        }
        #endregion

        //keypad buttons---------------------------------------------
        #region keypad buttons
        private void btn1_Click(object sender, EventArgs e)
        {
            keyEntry += "1";
            lblEntered.Text += (stars) ? "*" : "1";
            btnGo.Select();
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            keyEntry += "2";
            lblEntered.Text += (stars) ? "*" : "2";
            btnGo.Select();
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            keyEntry += "3";
            lblEntered.Text += (stars) ? "*" : "3";
            btnGo.Select();
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            keyEntry += "4";
            lblEntered.Text += (stars) ? "*" : "4";
            btnGo.Select();
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            keyEntry += "5";
            lblEntered.Text += (stars) ? "*" : "5";
            btnGo.Select();
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            keyEntry += "6";
            lblEntered.Text += (stars) ? "*" : "6";
            btnGo.Select();
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            keyEntry += "7";
            lblEntered.Text += (stars) ? "*" : "7";
            btnGo.Select();
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            keyEntry += "8";
            lblEntered.Text += (stars) ? "*" : "8";
            btnGo.Select();
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            keyEntry += "9";
            lblEntered.Text += (stars) ? "*" : "9";
            btnGo.Select();
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            keyEntry += "0";
            lblEntered.Text += (stars) ? "*" : "0";
            btnGo.Select();
        }

        private void btnBkSp_Click(object sender, EventArgs e)
        {
            if (keyEntry.Length > 0)
            {
                keyEntry = keyEntry.Substring(0, keyEntry.Length - 1);
                lblEntered.Text = lblEntered.Text.Substring(0, lblEntered.Text.Length - 1);
            }
            btnGo.Focus();
        }
        #endregion
        //-------------------------------------------------------------
        //Form buttons
        private void btnBack_Click(object sender, EventArgs e)
        {
            saveParams();
            if (zmqOpen)
            {
                if (!proc.HasExited || proc != null) { proc.Kill(); }
                receiver.Dispose();
                ctx.Dispose();
            }
            (new FrmStart()).Show();
            this.Hide();
        }
    }
}

# -*- coding: utf-8 -*-
"""
Created on Wed May 30 13:22:07 2018

@author: Joe
"""
#%%
#import scipy
import numpy as np
import cv2
#import time
#

#%%
class myBlobAnalyzerSide(object):
    def __init__(self):
        self.minBlobArea = 100;
        self.concavityThresh = 5;
        self.sideLookTresh = .30;
        
        self.percentFrameRemoveX = [.06,.47]
        self.percentFrameRemoveY = [.4,.04]
        
        self.sideViewStart = 190
        self.sideViewEnd = 255
        
        self.areaVec = np.zeros((500,1))
        
        self.maxBlobSize = 800
        self.clearEst = 0
        self.frameCount = 1
    
    def step(self,cImgIn):
        area = np.empty(0)
        centroids = np.empty((2,0))
        isFirst = True
        # make this a percentage of the frame
        cImg = cImgIn.copy()
        cImg[0:int(np.ceil(cImg.shape[0]*self.percentFrameRemoveY[0])),:] = 0
        cImg[-int(np.ceil(cImg.shape[0]*self.percentFrameRemoveY[1])):-1,:] = 0
        
        cImg[:,1:int(np.ceil(cImg.shape[1]*self.percentFrameRemoveX[0]))] = 0
        cImg[:,-int(np.ceil(cImg.shape[1]*self.percentFrameRemoveX[1])):-1] = 0
        
        bottomEdge = int(cImg.shape[0]*(1-self.percentFrameRemoveY[1])) - 3
        topEdge = int(cImg.shape[0]*self.percentFrameRemoveY[0]) + 3
        
        #print(topEdge)
        #print(bottomEdge)
        
        # MAKE side view
        sideView = cImgIn.copy();
        sideView[:,0:self.sideViewStart] = 0
        sideView[:,self.sideViewEnd:-1] = 0
        
        if np.any(cImg):
            #CH = convex_hull_object(cImg)
            #[cLabel,nObjects] = scipy.ndimage.measurements.label(cImg)
            #nObjects,cLabel = cv2.connectedComponents(cImg) # insludes background
            tmpcImg = 255*cImg.copy().astype('uint8')
            #tmpGarbage,contours,hierarchy = cv2.findContours(tmpcImg,0,1) # changed from ...(tmpcImg,2,1)
            #tmpGarbage,contours,hierarchy = cv2.findContours(tmpcImg,2,1) # changed from ...(tmpcImg,2,1)
            tmpGarbage,contoursAll,hierarchy = cv2.findContours(tmpcImg,cv2.RETR_TREE ,1) # changed from ...(tmpcImg,2,1)
            contours = [contoursAll[i] for i in range(len(contoursAll)) if hierarchy[:,i,-1] == -1]
            labelVec  = np.arange(0,len(contours))
            
            #startEval = time.time()
            #print(len(contours))
            for iObj in labelVec:
                cCont = contours[iObj]
                cHull = cv2.convexHull(cCont,returnPoints = False)
                defects = cv2.convexityDefects(cCont,cHull)
                #print(cCont[cHull])
                #print(cCont[4][0][1])
                #print(any([(cPnt[0][0] >= bottomEdge) or (cPnt[0][0] <= topEdge)   for cPnt in cCont]))
                #print(contYArray)
                if defects is not None:
                    contYArray = np.squeeze(np.array(cCont[cHull]))[:,1]
                    if np.any(contYArray >= bottomEdge) or np.any(contYArray <= topEdge): #any([(cPnt[0][1] >= bottomEdge) or (cPnt[0][1] <= topEdge)   for cPnt in cCont]): #cCont: # check to see is contour is overlapping with the edges
                        #print('on edge')
                        #print('time to check: ' + format(time.time() - startEval,'0.06'))
                        nDiffs = 1
                    else:
                        caveKeep = self.concavityThresh*256 < defects[:,:,3] # multiply by the bit size (256)
                        nDiffs = int(np.ceil(sum(caveKeep)[0]/2.0) + 1)
                        #if nDiffs > 1:
                        #    print('concavity: ' + str(nDiffs))
                else:
                        nDiffs = 1
                
                rows,cols = cImg.shape[:2]
                
                [vx,vy,x,y] = cv2.fitLine(cCont, cv2.DIST_L2,0,0.01,0.01)
                #lefty = int((-x*vy/vx) + y)
                #righty = int(((cols-x)*vy/vx)+y)
                cArea = cv2.contourArea(cCont)
                
                if cArea > .8*self.minBlobArea:
                    if isFirst:
                        centroids = np.array([x,y])
                        isFirst = False
                    else:
                        centroids = np.column_stack((centroids,np.array([x,y])))
                    
                    if nDiffs == 1 and self.frameCount > 35:
                        if cArea > 1.25*self.maxBlobSize:
                            #print('too big')
                            area = np.append(area,cArea/2)
                            centroids = np.column_stack((centroids,np.array([x,y])))
                            nDiffs = 2
                    elif nDiffs > 1:
                        for count in range(nDiffs - 1):
                            area = np.append(area,cArea/nDiffs)
                            centroids = np.column_stack((centroids,np.array([x,y])))
                    
                    area = np.append(area,cArea/nDiffs)

            #print('time to check:  {0:.06f}'.format(time.time() - startEval))
            if area.size > 0:
                if (self.frameCount<=500 and area.size > 0): # if we've seen 50 (500 frames of pills) pills, really no need to continue computation
                    self.maxBlobSize = np.max(np.append(area,self.maxBlobSize)) # take the max
                    self.areaVec[self.frameCount - 1] = np.max(area) # max of the ones in this frame
                    self.maxBlobSize = np.percentile(self.areaVec[0:self.frameCount], 98) # 98th percentile (reduce some outliers)
                    
                    self.frameCount = self.frameCount + 1 # increase framecount
            
                if (self.frameCount <= 50): # 50 frames of pills to check clear est
                    self.clearEst = self.clearEst + int((len(hierarchy[0]) - len(contours)) > 1) # create an estimate for how clear it is
                
        centroids = np.transpose(centroids)
        
        return area, centroids


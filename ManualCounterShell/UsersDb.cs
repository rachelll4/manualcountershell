﻿//Author: Rachel Logan
//Date: 6/12/19

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DPUruNet;

namespace ManualCounterShell
{
    //this was supposed to be a link between the fingerprint minutiae data and the user they belong to
    //not currently implemented
    class UsersDb
    {
        //private  List of USER objects--a name(f/l), an id number for fmd
        private List<User> users;
        private string saveFileName = "users.txt";

        private UsersDb()
        {
            users = new List<User>();
            if (File.Exists(saveFileName))
            {
                LoadDb();
            }
            else
            {
                File.Create(saveFileName);
            }
        }

        private void LoadDb()
        {
            string tempLine;
            string[] oneUsersData = new string[3]; //fname, lname, fmd ID
            try
            {
                using (StreamReader sr = new StreamReader(saveFileName))
                {
                    while (!sr.EndOfStream)
                    {
                        tempLine = sr.ReadLine();
                        oneUsersData = tempLine.Split(',');
                        add(oneUsersData[0], oneUsersData[1], oneUsersData[2]);
                    }
                }
            }
            catch
            {
            }
        }

        public void add (string fname, string lname, string FmdID)
        {
            users.Add(new User(fname, lname, FmdID));
            string written;
            try
            {
                using (StreamWriter sw = File.AppendText(saveFileName))
                {
                    written = fname + "," + lname + ',' + FmdID;
                    sw.WriteLine(written);
                }
            }
            catch
            {
            }
        }

        public string getName(string fmdId)
        {
            foreach (User user in users)
            {
                if (user.id() == fmdId)
                {
                    return user.name();
                }
            }
            return null;
        }

        private static UsersDb instance => new UsersDb();

        public static UsersDb Instance => instance;

        #region User class: fname, lname, fmid
        protected class User
        {
            private string firstname;
            private string lastname;
            private string FMDid; //corresponds to file saved for this user's print

            public User(string fname, string lname, string fmdid)
            {
                firstname = fname;
                lastname = lname;
                FMDid = fmdid;
            }

            public string name()
            {
                return firstname + " " + lastname;
            }

            public string id()
            {
                return FMDid;
            }
        }
        #endregion
    }
}

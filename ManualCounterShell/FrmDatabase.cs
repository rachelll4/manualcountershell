﻿//Author: Rachel Logan
//Date: 6/7/19

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManualCounterShell
{
    public partial class FrmDatabase : Form
    {
        public FrmDatabase()
        {
            InitializeComponent();
            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            formatListboxTabs();
            for (int c = Inventory.Instance.size() - 1; c >= 0; c--) //populate
            {
                lbItems.Items.Add(Inventory.Instance.strAt(c));
            }
        }

        private void formatListboxTabs()
        {
            lbItems.UseTabStops = true;
            lbItems.UseCustomTabOffsets = true;
            ListBox.IntegerCollection offsets = lbItems.CustomTabOffsets;
            offsets.Add(70);
            offsets.Add(110);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            (new FrmStart()).Show();
            this.Hide();
        }

        private void lbItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            //something selected, display its photo
            pbItemImage.Image = null;
            if (Inventory.Instance.getImageAt(Inventory.Instance.size() - 1 - lbItems.SelectedIndex) != null)
            {
                pbItemImage.Image = Inventory.Instance.getImageAt(Inventory.Instance.size() - 1 - lbItems.SelectedIndex);
            }
            //also display its data
            lblData.Text = Inventory.Instance.strDataAt(Inventory.Instance.size() - 1 - lbItems.SelectedIndex);
        }
    }
}

# -*- coding: utf-8 -*-
"""
Created on Thu Dec 13 22:12:59 2018
@author: admin (joe)

@author: Rachel Logan
Modified June 21, 2019
"""

import numpy as np
import zmq
from initPillUtil import initCam
from PIL import ImageTk, Image
import sys
import cv2
import datetime
import io
import base64

class videoGuiFuncs(object):

    def viewCameraFeedStart(self):
        print("acquiring params")
        #read in cam params from message
        msg = str(self.socket.recv_string()).split(",") #gives a list
        print(msg)
        print(msg[0])
        self.cameraROIheight=int(msg[0])
        self.cameraROIwidth=int(msg[1])
        self.cameraROItop=int(msg[2])
        self.cameraROIleft=int(msg[3])
        self.mainCropXleft=int(msg[4])
        self.mainCropXright=int(msg[5])
        self.mainCropYtop=int(msg[6])
        self.mainCropYbottom=int(msg[7])

        print("init'ing cam")
        self.cam = initCam(self.cameraROIheight,self.cameraROIwidth,self.cameraROIleft,self.cameraROItop)

        print("starting loop")
        self.viewCameraFeed()
    
    def viewCameraFeed(self):
        print("loop")
        skts=dict(self.poller.poll(50))
        if self.socket in skts:
            self.cam.close()
            return

        self.cam.snap_image(50)         # ask the camera to snap an image
        data, width, height, depth = self.cam.get_image_data() # retrieve the image off of the sensor
        print("image snapped")

        cFrameOg = np.flip(np.flip(np.ndarray(buffer=data,dtype=np.uint8,shape=(height, width, depth)),0),1)
        cFrameOg = np.flip(np.flip(cFrameOg,0),1)

        cFrame = np.zeros((cFrameOg.shape[0],cFrameOg.shape[1],1))
        cFrame[0:int(np.ceil(cFrame.shape[0]*self.mainCropYtop/100.0)),:] = 1
        cFrame[-int(np.ceil(cFrame.shape[0]*self.mainCropYbottom/100.0)):,:] = 1
        
        cFrame[:,0:int(np.ceil(cFrame.shape[1]*self.mainCropXleft/100.0))] = 1
        cFrame[:,-int(np.ceil(cFrame.shape[1]*self.mainCropXright/100.0)):] = 1
        
        cFrame = np.concatenate((cFrameOg,(155*(1-cFrame) + 100)),axis=2)#.astype('uint8')

        print("buffering")
        # Convert ndarray to base64 encoded string for jpg representation
        _, buffer = cv2.imencode('.jpg', cFrame ) #, [cv2.IMWRITE_JPEG_QUALITY, self.jpg_quality])
        jpg_enc = base64.b64encode(buffer).decode('utf-8')

        print("encoded to string: sending")
        self.socket.send_string(jpg_enc)#don't just send data bc processed #use str to send byte array!
        self.socket.send_string(str(height))
        self.socket.send_string(str(width))
        print("sent\n")

        self.viewCameraFeed()
        
    def __init__(self):
        #set up ZMQ socket etc
        sys.stdout = open('logs\mainVidLog.txt','a')
        print('{}'.format(datetime.datetime.now().strftime('%Y.%m.%d_%H.%M.%S')))
        sys.stdout.flush()

        context=zmq.Context()
        self.socket=context.socket(zmq.PAIR)
        self.socket.connect("tcp://127.0.0.1:5558")   #connects
        print("socket connected")

        self.poller=zmq.Poller()
        self.poller.register(self.socket,zmq.POLLIN) #only listens to socket

        #having it send one message back after initialization
        self.socket.recv()
        self.socket.send_string("received")
        print("comms work")

        while True:
            msg = self.socket.recv_string() #message will be 0,1 and filename OR 2 for camera feed
            msg=str(msg)
            index=msg[0]; #just first character #how to force int? #don't
    

            if index=='0': #loadVideo(filename) #gets all ready to start
                filename=msg[2:]; #everything besides first two chars
                latestFile = filename
                iFrame=0
                try:
                    latestFile = latestFile[:-4] + "_info.npy"
                    cCount =  np.load(latestFile)       #puts count into array list
                except:
                    cCount = np.zeros(15000)

            elif index=='1': #viewDispenseVideoStart(filename) #start
                iFrame += 1
                self.socket.send_string(str(cCount[iFrame])) 

            elif index=='2': #this is for camera feed
                print("message to show cam feed received")
                self.viewCameraFeedStart()
            elif index=='3':
                self.socket.send_string("closed")
                break
                #that should close cam bc broke out of poller loop
            else:
                self.socket.send_string("wrong index: " + index + "; " + msg)
                    #this is to speed up when nothing is changing in video
                    #if sum(sum(sum((startImage.astype('double') - cFrame.astype('double'))**2))) < 10000000.0:
                     #   timeDelay = 2
                    #else:
                    #    print('long')

                    #pause will just stop the play signal from being sent

    
        

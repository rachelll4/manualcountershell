# -*- coding: utf-8 -*-
"""
Created on Sun Aug  5 22:13:07 2018

@author: admin

modified for setting exposure time
101519
"""

from ic.pyicic.IC_ImagingControl import IC_ImagingControl,IC_GrabberDLL
from ctypes import *
import numpy as np


from myBlobAnalyzerSide import myBlobAnalyzerSide

#%%
class initTrackObj(object):
    def __init__(self,cam,xLeft,xRight,yTop,yBottom,cameraROIwidth,cameraROIheight, cameraChecks, minPillSetting):
        for i in range(4):
            try:
                self.initHelper(cam,xLeft,xRight,yTop,yBottom,cameraROIwidth,cameraROIheight, cameraChecks, minPillSetting)
                self.isBad = False
                return
            except Exception as e:
                print('camera error')
                print(e)
                self.isBad = True
        
            
    def initHelper(self,cam,xLeft,xRight,yTop,yBottom,cameraROIwidth,cameraROIheight,cameraChecks, minPillSetting):
        self.isDirty=False

        #%%   create background model
        time2Bg = 50 # in frames
        
        #frameBlock = np.zeros((252,272,time2Bg)).astype('uint8')
        frameBlock = np.zeros((cameraROIheight,cameraROIwidth,time2Bg)).astype('uint8')
        for i in range(time2Bg):
            cam.snap_image(1000)
            
            data, width, height, depth = cam.get_image_data()
            
            frame = np.ndarray(buffer=data,
                          dtype=np.uint8,
                          shape=(height, width, depth)) 
            
            frameBlock[:,:,i] = frame[:,:,1]
            
        self.backgroundModel = frameBlock.mean(axis = 2)
        
        #%% check for dirt
        frame = self.backgroundModel.astype(np.float)
        darkPix = np.sum(frame <= 25)/np.prod(frame.shape)
        if darkPix*100 > cameraChecks[0]:
            print('Too many black pixels')
            self.isDirty=True
            raise
            
        frame[frame <= 25] = 255
        frame[frame >= 230] = np.nan
        greyPix = np.sum(1-np.isnan(frame))/np.prod(frame.shape)
        
        if greyPix*100 > cameraChecks[1]:
            print('Too many dirty pixels')
            self.isDirty=True
            raise

        #%%
        # Create System objects used for reading video, detecting moving objects,
        # and displaying the results.
        self.bA = myBlobAnalyzerSide()
        
        #### change the frame cuts (this is for the side view included)
        #self.bA.percentFrameRemoveX = [.06,.47]
        #self.bA.percentFrameRemoveY = [.4,.04]
        #self.bA.percentFrameRemoveX = [.1,.43]
        #self.bA.percentFrameRemoveY = [.4,.1]
        
        #self.bA.percentFrameRemoveX = [.06,.45]
        #self.bA.percentFrameRemoveX = [.03,.40]
        #self.bA.percentFrameRemoveY = [.1,.001]
        
        self.bA.percentFrameRemoveX = [xLeft/100.0,xRight/100.0]
        self.bA.percentFrameRemoveY = [yTop/100.0,yBottom/100.0]
        
        self.bA.concavityThresh = 32#8#18#8#18#32
        self.bA.maxBlobSize = 0
        
        self.estVelStart = np.array([[0,15]],'double')  #[0,50].*(120/FPS).*(fliplr(size(frame)./[480,720]));
        
        self.tracks = []
        
        self.maxAssign = 80#65
        
        self.maxTime2Run = 0#200*60 # # in frames at 200 FPS
        self.countVec = np.zeros((self.maxTime2Run,1))
        self.frameTime = np.zeros((self.maxTime2Run,1))
        #waitTime = np.zeros((time2Run,1))
        #getTime = np.zeros((time2Run,1))
        
        time2Save = self.maxTime2Run
        self.frameStore = np.ascontiguousarray(np.zeros((cameraROIheight,cameraROIwidth,time2Save),'uint8'))
        
        cam.snap_image(50)
        cam.snap_image(50)
        data, width, height, depth = cam.get_image_data()
        
        np.ndarray(buffer=data,dtype=np.uint8,shape=(height, width, depth))[:,:,0];
        
        self.iFrame = 0
        self.curId = 1
        self.cam = cam
        self.curTime =0
        self.frameCheck = .005


def initCam(cameraROIheight,cameraROIwidth,cameraROIleft,cameraROItop,exposureTime):
    ic_ic = IC_ImagingControl()
    ic_ic.init_library()

    #%% get and start camera
    # open first available camera device
    cam_names = ic_ic.get_unique_device_names()
    cam = ic_ic.get_device(cam_names[0])
    cam.open()
    cam.reset_properties()

    # change camera properties
    #print(cam.list_property_names())         # ['gain', 'exposure', 'hue', etc...]
    cam.gain.auto = False                    # enable auto gain
    #tmp = IC_GrabberDLL.set_property_switch(cam._handle,c_char_p(b'Gain'),c_char_p(b'Auto'),c_int(0)) 
    #print("gain auto: "+str(tmp))
    cam.exposure.auto = False
    #tmp = IC_GrabberDLL.set_property_switch(cam._handle,c_char_p(b'Exposure'),c_char_p(b'Auto'),c_int(0)) 
    #print("exposure auto: "+str(tmp))
    #print(cam.exposure.range)                # (0, 10)
    #emin = cam.exposure.min                 # 0
    #emax = cam.exposure.max                 # 10
    #cam.exposure.value = -12#int((emin + emax) / 2)  # disables auto exposure and sets value to half of range
    #cam.exposure.value = exposureTime/1000.0
    tmp = IC_GrabberDLL.set_property_absolute_value(cam._handle,c_char_p(b'Exposure'),c_char_p(b'Value'),c_float(exposureTime/1000000.0)) 
    print("exposure value: "+str(tmp))
    #print(cam.exposure.value)                # 5

    #cam.gamma.value = 100

    # change camera settings
    formats = cam.list_video_formats()
    cam.set_video_format(formats[24])        # use 24st available video format
    cam.set_frame_rate(400)
    
    #h = cam.create_frame_filter(b'Rotate Flip')
    #cam.add_frame_filter_to_device(h)
    #cam.frame_filter_set_parameter(h,b'Rotation Angle', 180)
    
    h2 = cam.create_frame_filter(b'ROI')
    cam.add_frame_filter_to_device(h2)
    #cam.frame_filter_set_parameter(h2, b'Top', 117)
    #    cam.frame_filter_set_parameter(h2, b'Top', 137) # in the box version 1
    #    cam.frame_filter_set_parameter(h2, b'Left', 225)
    #    cam.frame_filter_set_parameter(h2, b'Height', 252)
    #    cam.frame_filter_set_parameter(h2, b'Width', 272)
    #frameSize = (267,312)
    #cam.frame_filter_set_parameter(h2, b'Height', frameSize[0])
    #cam.frame_filter_set_parameter(h2, b'Width', frameSize[1])
    #cam.frame_filter_set_parameter(h2, b'Top', 133)
    #cam.frame_filter_set_parameter(h2, b'Left', 200)
    
    cam.frame_filter_set_parameter(h2, b'Height', cameraROIheight)
    cam.frame_filter_set_parameter(h2, b'Width', cameraROIwidth)
    cam.frame_filter_set_parameter(h2, b'Top', cameraROItop)
    cam.frame_filter_set_parameter(h2, b'Left', cameraROIleft)

    # enable strobe
    tmp = IC_GrabberDLL.set_property_switch(cam._handle,c_char_p(b'Strobe'),c_char_p(b'Enable'),c_int(1)) 
    print("strobe on: "+str(tmp))
    tmp = IC_GrabberDLL.set_property_mapstring(cam._handle,c_char_p(b'Strobe'),c_char_p(b'Mode'),c_char_p(b'exposure')) 
    print("strobe mode: "+str(tmp))
    #tmp = IC_GrabberDLL.set_property_absolute_value(cam._handle,c_char_p(b'Strobe'),c_char_p(b'Duration'),c_float(exposureTime/1000000.0)) 

    #cam.enable_continuous_mode(True)        # image in continuous mode
    cam.start_live(show_display=False)       # start imaging

    cam.enable_trigger(False)                # camera will wait for trigger

    if not cam.callback_registered:
        cam.register_frame_ready_callback() # needed to wait for frame ready callback

    return cam
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManualCounterShell
{
    public class Definitions
    {
        //in millimeters
        //these are used to craft the debug window/border
        //this ensures that the app is portrayed on a simulated screen
        //which is the size of the monitor it will ultimately be displayed on
        //EDIT 
        private const int DEMO_SCREEN_WIDTH = 153; //machine the app is eventually going to run on, without the border
        private const int DEMO_SCREEN_HEIGHT = 92;
        private const int TRUE_SCREEN_WIDTH = 345; //machine the demo is displayed on
        private const int TRUE_SCREEN_HEIGHT = 195;

        //these represent percentages: xx.xxxxxF %
        //used in table layout panels in Designer.cs files
        //DO NOT EDIT
        public const float BORDER_WIDTH = 100 * (TRUE_SCREEN_WIDTH-DEMO_SCREEN_WIDTH) / (TRUE_SCREEN_WIDTH * 2);
        public const float DEMO_WIDTH = 100 * DEMO_SCREEN_WIDTH / TRUE_SCREEN_WIDTH;
        public const float BORDER_HEIGHT = 100 * (TRUE_SCREEN_HEIGHT-DEMO_SCREEN_HEIGHT) / (TRUE_SCREEN_HEIGHT * 2);
        public const float DEMO_HEIGHT = 100 * DEMO_SCREEN_HEIGHT / TRUE_SCREEN_HEIGHT;
    }
}

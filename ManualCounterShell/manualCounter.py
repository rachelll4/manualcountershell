#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Joe
Created on Thu Aug  2 12:09:58 2018

@author: Rachel Logan
Modified June 27 2019
"""
#%% imports
import pillUtil
#import time
from initPillUtil import initTrackObj,initCam
import csv
import tracker as tr
import numpy as np
import cv2
import datetime
import time
import sys
import zmq
from queue import Queue
from threading import Thread
import base64

#%%
# pyinstaller -F --add-data opencv_ffmpeg341_64.dll;. pythonInitial.py
#%%

class ManualCounter(object):
    def __init__(self):
        sys.stdout = open('logs\mainLog.txt','a')
        print('\n{}'.format(datetime.datetime.now().strftime('%Y.%m.%d_%H.%M.%S')))
        sys.stdout.flush()

        self.isConnectedToCount=False
        self.isConnectedToVideo=False
        self.isRunning = False
        self.isOn=True

        print("python is open")
        #################### here, start camera and run params ###################################
        ####### settable parameters (in setup)
        ##%%% These are all overwritten by the values in the parameterFilename %%%##
        self.parameterFilename = 'secureFile.txt' #have to make this
        
        self.minPillSize = 0                        # minimum pill size allowed
        self.minPillPercentage = 0                  # Percentage of Max pill size as min size
        self.cameraROIheight = 0                    # the height of the region cropped from the camera
        self.cameraROIwidth = 0                     # the width of the region cropped from the camera
        self.cameraROItop = 0                       # the margin from the top for the region cropped from the camera
        self.cameraROIleft = 0                      # the margin from the left for the region cropped from the camera
        self.mainCropXleft = 0                      # percentage crop from left of frame
        self.mainCropXright = 0                     # percentage crop from right of frame
        self.mainCropYtop = 0                       # percentage crop from top of frame
        self.mainCropYbottom = 0                    # percentage crop from bottom of frame
        self.exposureTime = 0                       # number of microseconds for camera to expose image
        self.numFramesCanMiss = 0                   # number of missed frames that can be tolerated
        self.blobSplitMax = 0                       # number of pills in a big blob to count correctly
        self.blackPixelCheck = 0                    # percentage of number of black pixels (standard frame is ~15%)
        self.greyPixelCheck = 0                     # percentage of grey pixels (shouldn't be more than 10%ish)
        self.lengthOfSavedMovie = 0                 # number of seconds to save for dispense videos
        self.logoSwitch = 0                         # 0:COL, 1:TCG # changes the logo to TCG or COL (you have to restart the program after changing this)
        self.flipImage = 0                          # setting for flipping image
        
        print("reading parameters")
        self.readAllParameters() # rewrite all the above parameters based on the saved setting found in parameterFilename
        sys.stdout.flush()
        #######
        
        self.trackObj = None
        self.bigPills = False
        self.dispenseMovie = None
        self.warning=False

        self.curCount = -1

        print("\ntime: "+str(time.time()))

        self.videoSaveQueue = Queue(maxsize = 0) # queue for saving the video
        videoSaveThread = Thread(target = self.checkVideoSaveQueue) # creat the threas
        videoSaveThread.daemon = True # make it close with the window--but window doesn't close--easier now bc close when socket disconnects
        videoSaveThread.start() # start thread (should be created off of the main thread)
        self.recording=False
        print("video save thread started")

        print("opening camera")
        self.openCam()

        ################## socket setup #####################################
        #setting up socket to receive ######################
        print("opening context, socket")
        #set up ZMQ socket etc
        self.context=zmq.Context()
        self.socket=self.context.socket(zmq.PAIR)
        print("context, socket created")
        self.socket.bind("tcp://127.0.0.1:5556")   #connects

        #setting up poller to watch for first signal #########################
        self.poller=zmq.Poller()
        self.poller.register(self.socket,zmq.POLLIN) #only listens to socket
        #this way, count can continue sending
        print("poller registered")
        sys.stdout.flush()
        #######################################################################

        #then enter loop to wait for connect
        print("isOn-------------------------")
        while self.isOn:
            #wait for connecting
            if (not self.isConnectedToCount and not self.isConnectedToVideo):
                skts=dict(self.poller.poll(50)) #50 milliseconds
                if self.socket in skts: #if a message comes through
                    msg=str(self.socket.recv_string())
                    print("message received: "+msg)
                    if msg=="both connected": #this is from frmcount or frminv
                        self.isConnectedToCount=True

                    elif msg=="vid connected": #this is from frmvideo
                        self.isConnectedToVideo=True
                    elif msg=="save":
                        self.cam.close()
                        self.readAllParameters()
                        self.cam=initCam(self.cameraROIheight,self.cameraROIwidth,self.cameraROIleft,self.cameraROItop,self.exposureTime)
                        self.socket.send_string("done")
                    else: #this is from settings
                        print("final close------------------------------------------------")
                        sys.stdout.close()
                        self.cam.close()
                        self.socket.send_string("closing")
                        self.socket.unbind("tcp://127.0.0.1:5556")
                        return
                else:
                    time.sleep(.05) #sleep 50 ms if no message to connect

            elif self.isConnectedToCount: #run start/init, then jumps into inventory mode
                self.counter=self.context.socket(zmq.PAIR)
                self.counter.connect("tcp://127.0.0.1:5557")
                self.counter.send_string("connected")
                print("new socket for counting done")
                sys.stdout.flush()
        
                self.clickStart()

            elif self.isConnectedToVideo: #going to look for video messages
                print("isVidConnected--------")
                while self.isConnectedToVideo:
                    skts=dict(self.poller.poll(1)) #1 millisecond
                    if self.socket in skts:
                        msg=str(self.socket.recv_string())
                        index=msg[0]; #just first character 

                        if index=='0': #load #gets all ready to start
                            filename=msg[2:]; #everything besides first two chars
                            latestFile = filename
                            iFrame=0
                            self.videoObj = cv2.VideoCapture(latestFile)

                            try:
                                latestFile = latestFile[:-4] + "_info.npy"
                                cCount =  np.load(latestFile) #puts count into array list
                            except:
                                cCount = np.zeros(15000)

                        elif index=='1': #viewDispenseVideoStart(filename) #start
                            isValid,cFrame = self.videoObj.read()
                            if isValid:
                                width,height=cFrame.shape[:2]
                                _, buffer = cv2.imencode('.jpg', cFrame ) #, [cv2.IMWRITE_JPEG_QUALITY, self.jpg_quality])
                                jpg_enc = base64.b64encode(buffer).decode('utf-8')

                                print("encoded to string: sending")
                                sys.stdout.flush()
                                self.socket.send_string(jpg_enc)#don't just send data bc processed #use str to send byte array!
                                self.socket.send_string(str(height))
                                self.socket.send_string(str(width))
                                print("sent\n")
                                iFrame += 1
                                self.socket.send_string(str(cCount[iFrame])) 
                            else:
                                self.socket.send_string("video over")
                                self.videoObj.release()

                        elif index=='2': #this is for camera feed
                            print("message to show cam feed received")
                            self.frameSender=self.context.socket(zmq.PAIR)
                            self.frameSender.bind("tcp://127.0.0.1:5557")
                            self.socket.send_string("done")
                            self.viewCameraFeed()
                        elif index=='3':
                            self.socket.send_string("closed")
                            self.isConnectedToVideo=False
    
############################ saving and updating parameters ##################
    def getParamList(self):
        with open(self.parameterFilename, mode='rb') as csv_file:
            decodeString = pillUtil.fileDecode('a8cc75d',csv_file.read().decode("utf-8-sig"))
            paramList = list(csv.reader(decodeString.split('\n'), delimiter=','))
        return paramList
    
    def readAllParameters(self,valIndex = 1): # val index using the 1: current or 2:default val in the csv to assign the variables
        paramList = self.getParamList()
            
        for rowI in range(len(paramList)):
            cRow = paramList[rowI]
            if len(cRow) > 1:
                exec('self.' + cRow[0] + '=' + cRow[valIndex])
    
    def writeAllParameters(self,write2Defaults = 0, justCam = False):
        paramList = self.getParamList()
        
        paramList = [cParam for cParam in paramList if len(cParam) > 0]
        for rowI in range(len(paramList)):
            if not justCam or (justCam and (paramList[rowI][0]=='cameraROIheight' or paramList[rowI][0]=='cameraROIwidth' or paramList[rowI][0]=='cameraROItop' or paramList[rowI][0]=='cameraROIleft' or paramList[rowI][0]=='mainCropXleft' or paramList[rowI][0]=='mainCropXright' or paramList[rowI][0]=='mainCropYtop' or paramList[rowI][0]=='mainCropYbottom')):
                paramList[rowI][1 + write2Defaults] = eval('self.' + paramList[rowI][0])
        
        with open('paramData.txt','w') as csv_file:
            cWriter = csv.writer(csv_file,lineterminator='\r')
            cWriter.writerows(paramList)
        
        with open('paramData.txt','r') as csv_file:
            encryptString = pillUtil.fileEncode('a8cc75d',csv_file.read())

        with open(self.parameterFilename,'wb') as csv_file:
            csv_file.write(encryptString.encode())
    
    def sendCom(self,message,returnBytes = 0,loopTime = 0,timeoutSet = .07):
        return None # incase someone is still trying to communicate
    
    ##################### Camera and FrmVideo functions ###############################
    def openCam(self): # open the camera connection
        self.cam = initCam(self.cameraROIheight,self.cameraROIwidth,self.cameraROIleft,self.cameraROItop,self.exposureTime) # initialize the camera (turn it on and initialize settings)
        # We'll use the camera object to snap all our images in the counting loops
    
    def viewCameraFeed(self):
        print("frameListener is: "+str(self.frameSender.recv_string()))
        while self.isConnectedToVideo:
            print("loop")
            skts=dict(self.poller.poll(50))
            if self.socket in skts:
                msg=str(self.socket.recv_string()) #3
                print("message received: " +msg)
                if msg=="0":
                    #send params
                    strToSend=str(self.cameraROIheight)+","+str(self.cameraROIwidth)+","+str(self.cameraROItop)+","+str(self.cameraROIleft)+","
                    strToSend+=str(self.mainCropXleft)+","+str(self.mainCropXright)+","+str(self.mainCropYtop)+","+str(self.mainCropYbottom)+","
                    strToSend+=str(self.exposureTime)
                    self.socket.send_string(strToSend)

                elif msg[0]=="1": #apply
                    self.incomingParams=msg[2:].split(',')
                    print(str(self.incomingParams[0]))
                    self.cameraROIheight=int(self.incomingParams[0])
                    self.cameraROIwidth=int(self.incomingParams[1])
                    self.cameraROItop=int(self.incomingParams[2])
                    self.cameraROIleft=int(self.incomingParams[3])
                    self.mainCropXleft=int(self.incomingParams[4])
                    self.mainCropXright=int(self.incomingParams[5])
                    self.mainCropYtop=int(self.incomingParams[6])
                    self.mainCropYbottom=int(self.incomingParams[7])
                    self.exposureTime=int(self.incomingParams[8])
                    print("params changed to "+str(self.cameraROIheight)+" "+str(self.cameraROIwidth)+" "+str(self.cameraROItop)+" "+str(self.cameraROIleft))
                    print("params changed to "+str(self.mainCropXleft)+" "+str(self.mainCropXright)+" "+str(self.mainCropYtop)+" "+str(self.mainCropYbottom))
                    print("params changed to "+str(self.exposureTime))
                    print("re-init cam w new params")
                    self.cam.close()
                    self.cam = initCam(self.cameraROIheight,self.cameraROIwidth,self.cameraROIleft,self.cameraROItop,self.exposureTime)
                    print("cam re-init'ed")
                    sys.stdout.flush()
                    strToSend=str(self.cameraROIheight)+","+str(self.cameraROIwidth)+","+str(self.cameraROItop)+","+str(self.cameraROIleft)+","
                    strToSend+=str(self.mainCropXleft)+","+str(self.mainCropXright)+","+str(self.mainCropYtop)+","+str(self.mainCropYbottom)+","
                    strToSend+=str(self.exposureTime)
                    self.socket.send_string(strToSend)
                elif msg[0]=="3":
                    if msg[1]=="a": #save to defaults
                        self.writeAllParameters(write2Defaults=1, justCam=True)
                        self.writeAllParameters()
                    elif msg[1]=="b": #just save
                        self.writeAllParameters()
                        print("written to param file")
                    self.readAllParameters()
                    self.frameSender.unbind("tcp://127.0.0.1:5557") 
                    self.isConnectedToVideo=False
                    self.socket.send_string("closed")
                    return
                elif msg=="frame":
                    print(str(self.cam.snap_image(50)))         # ask the camera to snap an image
                    data, width, height, depth = self.cam.get_image_data() # retrieve the image off of the sensor
                    print("image snapped")

                    cFrameOg = np.flip(np.flip(np.ndarray(buffer=data,dtype=np.uint8,shape=(height, width, depth)),0),1)
                    if self.flipImage:
                        cFrameOg = np.flip(np.flip(cFrameOg,0),1)

                    calcFrame = cFrameOg.astype(np.float)
                    blackPercent = int(100*np.sum(calcFrame <= 25)/np.prod(calcFrame.shape))
        
                    calcFrame[calcFrame<=25] = 255
                    calcFrame[calcFrame>=230] = np.nan
                    greyPercent = int(100*np.sum(1-np.isnan(calcFrame))/np.prod(calcFrame.shape))

                    cFrame = np.zeros((cFrameOg.shape[0],cFrameOg.shape[1],1))
                    cFrame[0:int(np.ceil(cFrame.shape[0]*self.mainCropYtop/100.0)),:] = 1
                    cFrame[-int(np.ceil(cFrame.shape[0]*self.mainCropYbottom/100.0)):,:] = 1
        
                    cFrame[:,0:int(np.ceil(cFrame.shape[1]*self.mainCropXleft/100.0))] = 1
                    cFrame[:,-int(np.ceil(cFrame.shape[1]*self.mainCropXright/100.0)):] = 1
        
                    cFrame = np.concatenate((cFrameOg,(155*(1-cFrame) + 100)),axis=2)#.astype('uint8')

                    print("buffering")
                    # Convert ndarray to base64 encoded string for jpg representation
                    _, buffer = cv2.imencode('.jpg', cFrame ) #, [cv2.IMWRITE_JPEG_QUALITY, self.jpg_quality])
                    jpg_enc = base64.b64encode(buffer).decode('utf-8')

                    print("encoded to string: sending")
                    self.frameSender.send_string(jpg_enc)#don't just send data bc processed #use str to send byte array!
                    self.frameSender.send_string(str(height))
                    self.frameSender.send_string(str(width))
                    self.frameSender.send_string('Black Pix: {0:02d}%, Grey: {1:02d}%'.format(blackPercent, greyPercent))
                    print("sent\n")

    ###################### Videos ##################################
    def checkVideoSaveQueue(self): # this loop is assigned to the videothread
        while True: # go forever
            cFrame = self.videoSaveQueue.get() # whenever anything comes onto the queue, go check the sensors (i.e., self.sensorQueue.put(1))
            try:
                if cFrame[0]: # is putting frames in
                    cInd =  np.argmax(self.dispenseMovie[0,0,:] == 0)
                    self.dispenseMovie[:,:,cInd] = cFrame[1]
                    self.dispenseMovie[0,0,cInd] = 1 # mark that this frame as been saved
                    self.dispenseMovie[0,1,cInd] = cFrame[3] # mark that this frame as been saved
                else:
                    self.saveDispenseMovie(cFrame[1]) # save the movie on seperate thread
            except:
                None
            self.videoSaveQueue.task_done() # tell the 

    def saveDispenseMovie(self,numDispensed):
        if self.lengthOfSavedMovie > 0: #Save movie if there is a length to the movie
            print("\tS: saving movie")
            cMovieBlock = self.dispenseMovie
            cCount = cMovieBlock[0,1,:]
            print(cCount)
            self.dispenseMovie = None
            dt = datetime.datetime.now()
            
            saveFileName = r'C:\Users\Robert\Desktop\saveDispenseVideos\{}_disp{}.mp4'.format(dt.strftime('%Y.%m.%d_%H.%M.%S'),numDispensed)            
            print(saveFileName)
            writer = cv2.VideoWriter(saveFileName, cv2.VideoWriter_fourcc(*"mp4v"),20.0,cMovieBlock.shape[1:None:-1],isColor=False)
            
            for iFrame in range(np.argmax(cMovieBlock[0,0,:] == 0)):
                writer.write(np.flip(np.flip(cMovieBlock[:,:,iFrame],0),1))
            writer.release()
            
            saveFileNameNpy = r'C:\Users\Robert\Desktop\saveDispenseVideos\{}_disp{}_info.npy'.format(dt.strftime('%Y.%m.%d_%H.%M.%S'),numDispensed)
            np.save(saveFileNameNpy,cCount)
            print("done saving")
            self.socket.send_string("done saving")

######################### inventory functions ##############################
    def clickStart(self):
        # check for running
        print("starting")
        sys.stdout.flush()
        if not self.isRunning:
            print("\nstarting")
            sys.stdout.flush()
            # set is running to true
            self.isRunning=True
            # restart the counter
            self.curCount=0

            #pre-alloc for movie saving
            if self.lengthOfSavedMovie > 0:
                self.dispenseMovie = np.zeros((self.cameraROIheight,self.cameraROIwidth,400*self.lengthOfSavedMovie),dtype ='uint8')

            # initInventory
            isReady = self.initInventory()

            print("initInv good: "+str(isReady))
            sys.stdout.flush()

            if isReady:
                # update label
                # start inventory routine
                self.socket.send_string("ready") #here, says done initializing
                sys.stdout.flush()
                self.inventoryPills()
            else:
                
                sys.stdout.flush()
                self.isRunning=False

    def initInventory(self):
        self.trackObj = None
        self.trackObj = initTrackObj(self.cam,self.mainCropXleft,self.mainCropXright,
                                                 self.mainCropYtop,self.mainCropYbottom,
                                                 self.cameraROIwidth,self.cameraROIheight,
                                                 [self.blackPixelCheck, self.greyPixelCheck],
                                                 [self.minPillSize, self.minPillPercentage]) # initilize the tracking object
        if self.trackObj.isBad:
            if self.trackObj.isDirty:
                self.socket.send_string("Warning: clean machine.")
            else:
                self.socket.send_string("1 Error: Check camera feed and reinitialize.")
            return False
        return True

    def inventoryPills(self):
        while(self.isConnectedToCount):
            skts=dict(self.poller.poll(.001))
            if self.socket in skts:
                msg=str(self.socket.recv_string())
                print("message received: "+msg)
                if msg[0]=='1': #SAVE MOVIE
                    print("saving video")
                    if self.lengthOfSavedMovie > 0 and self.recording:
                        self.videoSaveQueue.put((False,self.trackObj.curId - 1)) #sends done
                    else:
                        self.socket.send_string("no video to save")
                    self.isRunning=False
                    sys.stdout.flush()
                    self.recording=False
                elif msg[0]=='2': #cancel
                    print("restarting")
                    self.recording=False
                    self.isRunning=False
                    sys.stdout.flush()
                    print("clicking start")
                    sys.stdout.flush()
                    self.clickStart()
                elif msg[0]=='3': #no longer connected/active
                    print("closing\n")
                    sys.stdout.flush()
                    self.socket.send_string("closing")
                    self.isConnectedToCount=False
                    self.isRunning=False
                    self.counter.disconnect("tcp://127.0.0.1:5557") #ready to wait for new connection
                
            if (self.isRunning):
                startTime=time.time()
                numFrames = pillUtil.frameTime2Num(self.trackObj.frameCheck) # compute the number of frames we might have missed from the last loop
                ############################
                #### snap and get image ####
                try:
                    self.cam.snap_image(50)    # ask the camera to snap an image
                except:
                    print('camera offline')
                    sys.stdout.flush()
                    self.counter.send_string("1 Error: check camera") #handle as error
                    print("error sent")
                    self.isRunning=False
                    return

                data, width, height, depth = self.cam.get_image_data() # retrieve the image off of the sensor
                cFrame = np.ndarray(buffer=data,dtype=np.uint8,shape=(height, width, depth))[:,:,0] # convert the data from the sensor to a numpy array
                ############################
        
                #### preprocessing the current frame ####
                frame = pillUtil.preProcFrame(cFrame,self.trackObj.backgroundModel)
        
                if self.flipImage:
                    frame = np.flip(np.flip(frame,0),1)

                #### send the frame through the tracker (tr) ###
                self.trackObj.tracks,nextId = tr.step(frame,self.trackObj.bA,self.trackObj.tracks,self.trackObj.maxAssign,self.trackObj.curId,self.trackObj.estVelStart,numFrames)

                ### updating the timing for the latest and first pill
                if (nextId - self.trackObj.curId) != 0:
                    lastNewPillTime = time.time() # when the last new pill fell through 
                    print(datetime.datetime.now().strftime('%Y.%m.%d_%H:%M:%S.%f') + ' Pill: ' + str(self.trackObj.curId))
                    sys.stdout.flush()
                if self.trackObj.curId == 1 and nextId != 1: # if this is the first pill, record the time
                    self.recording=True #var for when to start video
                    print("recording is on")

                if ((not self.trackObj.bA.frameCount%150) or self.trackObj.bA.frameCount == 45): #at the 45th frame with pills
                    if self.trackObj.bA.maxBlobSize > 5000.0:
                        self.bigPills = True
            
                    realMaxBlob = self.trackObj.bA.maxBlobSize.copy()
            
                    if self.trackObj.bA.clearEst > 10: # clear estimate
                        self.trackObj.bA.maxBlobSize = self.trackObj.bA.maxBlobSize - 500.0
                        self.trackObj.bA.concavityThresh = 100
            
                    self.trackObj.bA.maxBlobSize = realMaxBlob
                    if self.trackObj.bA.frameCount == 45:
                        print('max blob: ' + str(self.trackObj.bA.maxBlobSize))
                        print('min blob: ' + str(self.trackObj.bA.minBlobArea))
                ### increase the frame counter and update what the next pill ID will be
                self.trackObj.iFrame += 1
                self.trackObj.curId = nextId
        
                # { NEW 8/25/19
                if not self.warning:
                    if self.numFramesCanMiss < numFrames:
                        self.counter.send_string("Warning: Possible Miscount\nMissed some video frames")
                        print('{}: missed frames'.format(datetime.datetime.now().strftime('%Y.%m.%d_%H.%M.%S')))
                        self.warning=True
                    if self.blobSplitMax < self.trackObj.bA.blobSplit:
                        self.counter.send_string("Warning: Possible Miscount\nPills are too bunched")
                        print('{}: pills bunched'.format(datetime.datetime.now().strftime('%Y.%m.%d_%H.%M.%S')))
                        self.warning=True
                #}

                if (self.lengthOfSavedMovie > 0 and self.recording): # and not (self.topSensePillTime == 0): # saving longer movies now
                    self.videoSaveQueue.put((True,cFrame,datetime.datetime.now().strftime('%Y.%m.%d_%H:%M:%S.%f'),(self.trackObj.curId - 1)))
        
                ### update the current count IN LABEL--here, send on up
                if not self.trackObj.iFrame%70: # every 70 frames update the current count 
                    self.curCount=self.trackObj.curId - 1
                    self.counter.send_string("0 "+str(self.curCount))

                ### record the time it took to get through the loop
                self.trackObj.frameCheck = time.time() - startTime        
                #if self.trackObj.frameCheck < .002: # if it was too short, wait an extra 2ms
                #    delay += 0#2
                if self.trackObj.frameCheck > .007: # if the loop was too long, tell the console
                    print('Frame {} time over: {} m-secs'.format(self.trackObj.iFrame,self.trackObj.frameCheck*1000))
                    sys.stdout.flush()
    

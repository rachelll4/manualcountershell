﻿namespace ManualCounterShell
{
    partial class FrmStart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpBorder = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSetup = new System.Windows.Forms.Button();
            this.btnMdCount = new System.Windows.Forms.Button();
            this.btnMdInv = new System.Windows.Forms.Button();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.btnDb = new System.Windows.Forms.Button();
            this.lblCOL = new System.Windows.Forms.Label();
            this.lblHeading = new System.Windows.Forms.Label();
            this.tlpBorder.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpBorder
            // 
            this.tlpBorder.BackColor = System.Drawing.Color.LightSlateGray;
            this.tlpBorder.ColumnCount = 5;
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18F));
            this.tlpBorder.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tlpBorder.Controls.Add(this.tableLayoutPanel2, 1, 1);
            this.tlpBorder.Controls.Add(this.lblCOL, 1, 0);
            this.tlpBorder.Controls.Add(this.lblHeading, 2, 0);
            this.tlpBorder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpBorder.Location = new System.Drawing.Point(0, 0);
            this.tlpBorder.Name = "tlpBorder";
            this.tlpBorder.RowCount = 3;
            this.tlpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tlpBorder.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpBorder.Size = new System.Drawing.Size(1317, 710);
            this.tlpBorder.TabIndex = 3;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.DarkSlateGray;
            this.tableLayoutPanel2.ColumnCount = 7;
            this.tlpBorder.SetColumnSpan(this.tableLayoutPanel2, 3);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tableLayoutPanel2.Controls.Add(this.btnSetup, 5, 5);
            this.tableLayoutPanel2.Controls.Add(this.btnMdCount, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnMdInv, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblWelcome, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnDb, 5, 3);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(16, 74);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 7;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1283, 562);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // btnSetup
            // 
            this.btnSetup.BackColor = System.Drawing.Color.Thistle;
            this.btnSetup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSetup.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.btnSetup.ForeColor = System.Drawing.Color.Navy;
            this.btnSetup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSetup.Location = new System.Drawing.Point(809, 411);
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.Size = new System.Drawing.Size(404, 134);
            this.btnSetup.TabIndex = 1;
            this.btnSetup.Text = "Setup";
            this.btnSetup.UseVisualStyleBackColor = false;
            this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
            // 
            // btnMdCount
            // 
            this.btnMdCount.BackColor = System.Drawing.Color.Thistle;
            this.btnMdCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnMdCount.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.btnMdCount.ForeColor = System.Drawing.Color.Navy;
            this.btnMdCount.Location = new System.Drawing.Point(67, 260);
            this.btnMdCount.Name = "btnMdCount";
            this.tableLayoutPanel2.SetRowSpan(this.btnMdCount, 3);
            this.btnMdCount.Size = new System.Drawing.Size(340, 285);
            this.btnMdCount.TabIndex = 3;
            this.btnMdCount.Text = "Count mode";
            this.btnMdCount.UseVisualStyleBackColor = false;
            this.btnMdCount.Click += new System.EventHandler(this.btnMdCount_Click);
            // 
            // btnMdInv
            // 
            this.btnMdInv.BackColor = System.Drawing.Color.Thistle;
            this.btnMdInv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnMdInv.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.btnMdInv.ForeColor = System.Drawing.Color.Navy;
            this.btnMdInv.Location = new System.Drawing.Point(425, 260);
            this.btnMdInv.Name = "btnMdInv";
            this.tableLayoutPanel2.SetRowSpan(this.btnMdInv, 3);
            this.btnMdInv.Size = new System.Drawing.Size(340, 285);
            this.btnMdInv.TabIndex = 2;
            this.btnMdInv.Text = "Inventory mode";
            this.btnMdInv.UseVisualStyleBackColor = false;
            this.btnMdInv.Click += new System.EventHandler(this.btnMdInv_Click);
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.lblWelcome, 5);
            this.lblWelcome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWelcome.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold);
            this.lblWelcome.ForeColor = System.Drawing.Color.Thistle;
            this.lblWelcome.Location = new System.Drawing.Point(67, 56);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(1146, 168);
            this.lblWelcome.TabIndex = 4;
            this.lblWelcome.Text = "Welcome! Select mode";
            this.lblWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDb
            // 
            this.btnDb.BackColor = System.Drawing.Color.Thistle;
            this.btnDb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDb.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold);
            this.btnDb.ForeColor = System.Drawing.Color.Navy;
            this.btnDb.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDb.Location = new System.Drawing.Point(809, 260);
            this.btnDb.Name = "btnDb";
            this.btnDb.Size = new System.Drawing.Size(404, 134);
            this.btnDb.TabIndex = 6;
            this.btnDb.Text = "Database";
            this.btnDb.UseVisualStyleBackColor = false;
            this.btnDb.Click += new System.EventHandler(this.btnDb_Click);
            // 
            // lblCOL
            // 
            this.lblCOL.AutoSize = true;
            this.lblCOL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCOL.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCOL.ForeColor = System.Drawing.Color.Black;
            this.lblCOL.Location = new System.Drawing.Point(16, 0);
            this.lblCOL.Name = "lblCOL";
            this.lblCOL.Size = new System.Drawing.Size(125, 71);
            this.lblCOL.TabIndex = 6;
            this.lblCOL.Text = "COL";
            this.lblCOL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblHeading
            // 
            this.lblHeading.AutoSize = true;
            this.lblHeading.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHeading.Font = new System.Drawing.Font("Verdana", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeading.ForeColor = System.Drawing.Color.Black;
            this.lblHeading.Location = new System.Drawing.Point(147, 0);
            this.lblHeading.Name = "lblHeading";
            this.lblHeading.Size = new System.Drawing.Size(915, 71);
            this.lblHeading.TabIndex = 5;
            this.lblHeading.Text = "Manual Counter Workstation";
            this.lblHeading.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrmStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(1317, 710);
            this.Controls.Add(this.tlpBorder);
            this.Name = "FrmStart";
            this.Text = "FrmStart";
            this.tlpBorder.ResumeLayout(false);
            this.tlpBorder.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpBorder;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnSetup;
        private System.Windows.Forms.Button btnMdCount;
        private System.Windows.Forms.Button btnMdInv;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.Button btnDb;
        private System.Windows.Forms.Label lblCOL;
        private System.Windows.Forms.Label lblHeading;
    }
}
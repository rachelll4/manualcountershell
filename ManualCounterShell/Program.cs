﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Reflection.Assembly;
using System.IO;

namespace ManualCounterShell
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            LoadFrom("Interop.CoreScanner.dll");
            LoadFrom("DPUruNet.dll");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //before run form, find/check scanner toggles
            Inventory.Instance.readInParams();
            Inventory.Instance.runPythonCountScript();

            if (Inventory.Instance.getFingerScanIsOn())
                Application.Run(new FrmLogin());
            else
                Application.Run(new FrmStart());
        }
    }
}

﻿//Author: Rachel Logan
//Date: 6/20/19

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZeroMQ;
using System.Drawing.Imaging;
using System.Threading;
using System.Runtime.InteropServices;

namespace ManualCounterShell
{
    public partial class FrmVideo : Form
    {
        string filename;

        ZContext ctx;
        ZSocket receiver;
        ZSocket frameListener;

        bool zmqOpen;

        bool running;
        bool displaying;
        bool always;
        bool originalRun;
        Thread runVideo;
        Thread feedVideo;
        int auth;

        int count;
        int numLoop;
        int frameCount;

        bool feedView;
        bool tintView;
        int msPerFrame;
        int paramIndex;

        int[] ROIhwtl_CROPlrtb;
        int[] paramchanges;
        string[] camParamNames;

        public FrmVideo()
        {
            InitializeComponent();
            zmqOpen = false;
            running = false;
            originalRun = true;
            always = true;
            tintView = true;
            paramIndex = -1;
            feedView = Inventory.Instance.getFeedView();

            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;

            btnPlay.Visible = false; //not visible until one selected
            btnPause.Visible = false;
            btnSlow.Visible = false;
            btnFast.Visible = false;
            btnSave.Visible = false;
            btnSaveDefault.Visible = false;

            msPerFrame = 50; //time between next frame grab

            if (!feedView)
            {
                populateList();
                lbVideos.SelectedIndexChanged += lbVideos_OnSelectedIndexChanged;
            }
            else //feedview active
            {
                ROIhwtl_CROPlrtb = new int[9];
                paramchanges = new int[9];
                camParamNames = new string[] { "Camera ROI Height", "Camera ROI Width", "Camera ROI Top", "Camera ROI Left",
                "Main Crop % Left", "Main Crop % Right", "Main Crop % Top", "Main Crop % Bottom","Exposure Time [micro-sec]"};

                lblData.Text = "Displaying camera feed.";
                btnSlow.Text = "Frame";
                btnSlow.Visible = true;
                btnPause.Text = "v";
                btnPlay.Text = "^";
                btnFast.Text = "Apply";
                Thread.Sleep(650);
                ZMQinit();
                receiver.SendFrame(new ZFrame("2")); //camera feed
                receiver.ReceiveFrame().ReadString(); //done before open frameListener

                displaying = true;
                feedVideo = new Thread(DisplayFeed);
                feedVideo.Start();
                
                receiver.SendFrame(new ZFrame("0")); //send params
                                                     //get params from python and set into arrays?
                ROIhwtl_CROPlrtb = Array.ConvertAll(receiver.ReceiveFrame().ReadString().Split(','), int.Parse);
                auth = Inventory.Instance.checkStillLoggedInAuth();
                if (auth== 2)
                {
                    //then put names into listbox
                    for (int c = 0; c < 9; c++)
                    {
                        paramchanges[c] = ROIhwtl_CROPlrtb[c];
                        lbVideos.Items.Add(camParamNames[c] + ": " + ROIhwtl_CROPlrtb[c].ToString());
                    }
                    btnBack.Text = "Don't save";
                    btnSave.Visible = true;
                    btnSaveDefault.Visible = true;
                }
            }
        }
        //--------------------------------------------------------------------------------------------
        private void DisplayFeed()
        {
            //here, receive quartets, translate to image, show in box
            //zmq already built
            frameListener = new ZSocket(ctx, ZSocketType.PAIR);
            frameListener.Connect("tcp://127.0.0.1:5557");
            frameListener.SendFrame(new ZFrame("connected"));
            while (always)
            {
                if (displaying) //goes until exit form
                {
                    receiver.SendFrame(new ZFrame("frame"));
                    byte[] bytebuffer = Convert.FromBase64String(frameListener.ReceiveFrame().ReadString());
                    int height = int.Parse(frameListener.ReceiveFrame().ReadString());
                    int width = int.Parse(frameListener.ReceiveFrame().ReadString());
                    string msg = frameListener.ReceiveFrame().ReadString();
                    lblPixelCheck.BeginInvoke((MethodInvoker)delegate { lblPixelCheck.Text = msg; });
                    using (MemoryStream ms = new MemoryStream(bytebuffer)) //image data
                    {
                        Bitmap img = new Bitmap(Image.FromStream(ms), new Size(height, width));
                        if (tintView) img = applyTint(img);
                        pbVideo.BeginInvoke((MethodInvoker)delegate
                        {
                            pbVideo.Image = img;
                        });
                    }
                }
                else
                {
                    Thread.Sleep(80);
                }
            }
        }

        private void DisplayFrames()
        {
            numLoop = 0;
            int prevCount = 0;
            frameCount = 0;
            Bitmap temp;
            while (always)
            {
                if (running)
                {
                    //Bitmap temp = vidReader.ReadVideoFrame();
                    //instead, receive from python
                    receiver.SendFrame(new ZFrame("1 " + filename)); //1 means send FRAME and count

                    string hopeful = receiver.ReceiveFrame().ReadString();
                    if (hopeful == "video over")
                    {
                        running = false;
                        lblData.BeginInvoke((MethodInvoker)delegate { lblData.Text = "Video over"; btnPlay.Visible = true; btnPlay.Text = "Play"; btnPause.Visible = false; });
                        originalRun = true;
                        always = false;
                        //release video
                    }
                    else
                    {
                        byte[] bytebuffer = Convert.FromBase64String(hopeful);
                        int height = int.Parse(receiver.ReceiveFrame().ReadString());
                        int width = int.Parse(receiver.ReceiveFrame().ReadString());

                        using (MemoryStream ms = new MemoryStream(bytebuffer)) //image data
                        {
                            temp = new Bitmap(Image.FromStream(ms), new Size(height, width));
                        }
                        if (Inventory.Instance.getFlipCamera())
                            temp.RotateFlip(RotateFlipType.RotateNoneFlipXY);
                        pbVideo.BeginInvoke((MethodInvoker)delegate { pbVideo.Image = temp; });
                        count = int.Parse(receiver.ReceiveFrame().ReadString());
                        if (prevCount > count)
                        {
                            numLoop++;
                        }
                        prevCount = count;
                        pbVideo.BeginInvoke((MethodInvoker)delegate
                        {
                            lblData.Text = "Count: " + (count + 256 * numLoop) + "; FPS: " + 1000 / msPerFrame + "; Frame: " + ++frameCount;
                            pbVideo.Image = temp;
                        });
                        Thread.Sleep(msPerFrame);
                    }
                }
            }
        }

        private void btnPlay_Click(object sender, EventArgs e) //only not feedview
        {
            if (!feedView)
            {
                btnPause.Visible = true;
                btnPlay.Visible = false;
                btnFast.Visible = true;
                btnSlow.Visible = true;

                running = true;
                if (originalRun)
                {
                    receiver.SendFrame(new ZFrame("0 " + filename)); //0 reloads array of counts

                    numLoop = 0;
                    always = true;
                    runVideo = new Thread(DisplayFrames);
                    runVideo.Start();
                }
                originalRun = false;
            }
            else //param increases
            {
                paramchanges[paramIndex]++;
                lbVideos.Items[paramIndex] = camParamNames[paramIndex] + ": " + ROIhwtl_CROPlrtb[paramIndex] + "->" + paramchanges[paramIndex];
                if (!btnFast.Visible) btnFast.Visible = true;
            }
        }

        private void btnPause_Click(object sender, EventArgs e) //only not feedview
        {
            if (!feedView)
            {
                running = false;
                btnPause.Visible = false;
                btnPlay.BeginInvoke((MethodInvoker)delegate { btnPlay.Text = "Resume"; btnPlay.Visible = true; });
            }
            else //param decreases
            {
                paramchanges[paramIndex]--;
                lbVideos.Items[paramIndex] = camParamNames[paramIndex] + ": " + ROIhwtl_CROPlrtb[paramIndex].ToString() + "->" + paramchanges[paramIndex].ToString();
                if (!btnFast.Visible) btnFast.Visible = true;
            }
        }

        private void lbVideos_OnSelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (lbVideos.SelectedIndex < 0) return;

            if (feedView)
            {
                btnPlay.Visible = true;
                btnPause.Visible = true;
                paramIndex = lbVideos.SelectedIndex;
                return;
            }

            always = false; //kills running displayFrame thread
            //msPerFrame = 50; //reset time between frames for new video
            running = false;
            numLoop = 0;
            count = 0;
            originalRun = true;

            if (!zmqOpen) { ZMQinit(); }
            else { Thread.Sleep(250); }

            filename = "C:\\Users\\Robert\\Desktop\\saveDispenseVideos\\" + lbVideos.SelectedItem.ToString();

            long length = new System.IO.FileInfo(filename).Length;
            if (length < 5000) //if too small, it's corrupted, didn't save right
            {
                lblData.Text = "File not loaded: save failed.";
                btnPlay.Visible = false;
                pbVideo.Image = null;
                return;
            }

            //here, load filename and send "0 filename"
            receiver.SendFrame(new ZFrame("0 " + filename)); //0 loads array of counts
            receiver.SendFrame(new ZFrame("1 " + filename));

            byte[] bytebuffer = Convert.FromBase64String(receiver.ReceiveFrame().ReadString());
            int height = int.Parse(receiver.ReceiveFrame().ReadString());
            int width = int.Parse(receiver.ReceiveFrame().ReadString());

            using (MemoryStream ms = new MemoryStream(bytebuffer)) //image data
            {
                Bitmap temp = new Bitmap(Image.FromStream(ms), new Size(height, width));
                if (Inventory.Instance.getFlipCamera())
                    temp.RotateFlip(RotateFlipType.RotateNoneFlipXY);
                pbVideo.BeginInvoke((MethodInvoker)delegate { pbVideo.Image = temp; });
            }

            lblData.Text = "Count: " + receiver.ReceiveFrame().ReadString();


            lblData.Text = "Loaded.";
            btnPlay.Text= "Play";
            btnPlay.Visible = true;
            btnPause.Visible = false;
            btnSlow.Visible = false;
            btnFast.Visible = false;
        }

        #region Initialize ZMQ, list, python
        private void ZMQinit()
        {
            ctx = new ZContext();
            receiver = new ZSocket(ctx, ZSocketType.PAIR);

            receiver.Connect("tcp://127.0.0.1:5556"); //same "address" as in manualCounter
            receiver.SendFrame(new ZFrame("vid connected"));
            //send to running py file and receive back, to check

            zmqOpen = true;
        }

        private void populateList() //only not feedview
        {
            lbVideos.Items.Clear();
            string[] paths = Directory.GetFiles("C:/Users/Robert/Desktop/saveDispenseVideos/","*.mp4");
            Array.Reverse(paths); //put more recent items at the top
            foreach (string path in paths){
                lbVideos.Items.Add(Path.GetFileName(path));
            }
        }

        #endregion

        //-------------------------------------------------------------
        #region Form buttons

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (zmqOpen)
            {
                receiver.SendFrame(new ZFrame("3c"));
                receiver.ReceiveFrame();

                if (runVideo != null ) { running = false; runVideo.Abort(); }
                if (displaying) { feedVideo.Abort(); frameListener.Dispose(); }
                receiver.Dispose();
                ctx.Dispose();
            }
            Inventory.Instance.login(auth);
            Inventory.Instance.setFeedView(false); //turns off feed view
            (new FrmSetup()).Show();
            this.Hide();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            always = false;
            Thread.Sleep(120);
            receiver.SendFrame(new ZFrame("3b"));

            receiver.ReceiveFrame().ReadString();
            if (displaying) { feedVideo.Abort(); frameListener.Disconnect("tcp://127.0.0.1:5557"); frameListener.Dispose(); }

            frameListener.Dispose();
            receiver.Dispose();
            ctx.Dispose();

            Inventory.Instance.setFeedView(false); //turns off feed view
            (new FrmSetup()).Show();
            this.Hide();
        }

        private void btnSaveDefault_Click(object sender, EventArgs e)
        {
            always = false;
            Thread.Sleep(120);
            receiver.SendFrame(new ZFrame("3a"));

            receiver.ReceiveFrame().ReadString();

            if (displaying) { feedVideo.Abort(); frameListener.Disconnect("tcp://127.0.0.1:5557"); frameListener.Dispose(); }
            frameListener.Dispose();
            receiver.Dispose();
            ctx.Dispose();
            
            Inventory.Instance.setFeedView(false); //turns off feed view
            (new FrmSetup()).Show();
            this.Hide();
        }

        private void btnSlow_Click(object sender, EventArgs e)
        {
            if (!feedView)
            {
                if (msPerFrame < 200)
                    msPerFrame += 10;
                lblData.BeginInvoke((MethodInvoker)delegate { lblData.Text = "Count: " + (count + 256 * numLoop) + "; FPS: " + 1000 / msPerFrame + "; Frame: " + frameCount; });
                return;
            }
            tintView = !tintView;
        }
        private void btnFast_Click(object sender, EventArgs e)
        {
            if (!feedView)
            {
                if (msPerFrame > 10)
                    msPerFrame -= 10;
                lblData.BeginInvoke((MethodInvoker)delegate { lblData.Text = "Count: " + (count + 256 * numLoop) + "; FPS: " + 1000 / msPerFrame + "; Frame: " + frameCount; });
                return;
            }
            displaying = false;
            Thread.Sleep(100);
            receiver.SendFrame(new ZFrame("1 " + paramchanges[0].ToString() + "," + 
                paramchanges[1].ToString() + "," + paramchanges[2].ToString() + "," + 
                paramchanges[3].ToString()+","+ paramchanges[4].ToString()+","+
                paramchanges[5].ToString()+","+ paramchanges[6].ToString()+","+
                paramchanges[7].ToString()+","+paramchanges[8].ToString()));

            lbVideos.Items.Clear();
            ROIhwtl_CROPlrtb = Array.ConvertAll(receiver.ReceiveFrame().ReadString().Split(','), int.Parse);
            //then put names into listbox
            for (int c = 0; c < 9; c++)
            {
                paramchanges[c] = ROIhwtl_CROPlrtb[c];
                lbVideos.Items.Add(camParamNames[c] + ": " + ROIhwtl_CROPlrtb[c].ToString());
            }

            btnFast.Visible = false;
            btnPause.Visible = false;
            btnPlay.Visible = false;
            btnSlow.Visible = true;
            displaying = true;
        }

        #endregion

        private Bitmap applyTint(Bitmap sourceBitmap)
        {
            if (!Inventory.Instance.getFlipCamera())
                sourceBitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
            BitmapData sourceData = sourceBitmap.LockBits(new Rectangle(0, 0,
                            sourceBitmap.Width, sourceBitmap.Height),
                            ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

            //float blueTint = .5F;

            byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];
            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);
            sourceBitmap.UnlockBits(sourceData);

            float blue = 0;
            float red = 0;
            float green = 0;

            for (int k = 0; k + 4 < pixelBuffer.Length; k += 4)
            {
                if (k < (sourceBitmap.Width *4 * (ROIhwtl_CROPlrtb[6] * .01 * sourceBitmap.Height)) || //top
                    (k > (sourceBitmap.Width *4* ((1 - ROIhwtl_CROPlrtb[7] * .01) * sourceBitmap.Height))) || //bottom
                    (k % (sourceBitmap.Width*4) < sourceBitmap.Width *4* ROIhwtl_CROPlrtb[4] * .01) || //left edge
                    (k % (sourceBitmap.Width*4) > sourceBitmap.Width *4* (1-ROIhwtl_CROPlrtb[5] * .01))) //right edge
                {

                    //pixelBuffer[k] /= 2; //halfs intensity, to tint white
                    //blue = pixelBuffer[k] + (255 - pixelBuffer[k]) * blueTint;
                    blue = pixelBuffer[k] + 172;
                    green = pixelBuffer[k + 1] - 64;
                    red = pixelBuffer[k + 2] - 64;

                    if (blue > 255)
                    { blue = 255; }
                    if (red < 0) red = 0;
                    if (green < 0) green = 0;

                    pixelBuffer[k] = (byte)blue;
                    pixelBuffer[k + 1] = (byte)green;
                    pixelBuffer[k + 2] = (byte)red;
                }
            }

            Bitmap resultBitmap = new Bitmap(sourceBitmap.Width, sourceBitmap.Height);

            BitmapData resultData = resultBitmap.LockBits(new Rectangle(0, 0,
                                    resultBitmap.Width, resultBitmap.Height),
                                    ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

            Marshal.Copy(pixelBuffer, 0, resultData.Scan0, pixelBuffer.Length);
            resultBitmap.UnlockBits(resultData);

            if (!Inventory.Instance.getFlipCamera())
                resultBitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);

            return resultBitmap;
        }

    }
}

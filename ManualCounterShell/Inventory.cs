﻿//Author: Rachel Logan
//Date: 6/11/19

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace ManualCounterShell
{
    //this class is for saving prescriptions with images and counts. 
    //will be linked to ndc database in future
    class Inventory
    {
        private static List<InvItem> list;
        private int length;
        private string saveFileName = "database.txt";

        public string nameOfCurrentPharmacist="Guest"; //could do get/set if felt like

        private bool BarcodeScannerIsOn;
        private bool FingerScannerIsOn;

        private bool feedView=false;
        private bool flipImage;

        private int authLevel;
        private DateTime lastlogin;

        private Process pyCountProcess;

        private Inventory()
        {
            list = new List<InvItem>();
            length = 0;
            if (File.Exists(saveFileName))
            {
                LoadDatabase();
            }
            else
            {
                File.Create(saveFileName); //empty so nothing to load in
            }
        }

        //parameter/camera/python functions-------------------------------------
        public int checkStillLoggedInAuth()
        {
            if (DateTime.Now - lastlogin < TimeSpan.FromMinutes(10)) 
                return authLevel;
            else
                return 0;
        }
        public void login(int auth)
        {
            authLevel = auth;
            lastlogin = DateTime.Now;
        }

        #region scanner toggles
        public void toggleBarcodeScan()
        {
            BarcodeScannerIsOn = !BarcodeScannerIsOn;
        }

        public void toggleFingerScan()
        {
            FingerScannerIsOn = !FingerScannerIsOn;
        }
        public void toggleFlipImage()
        {
            flipImage = !flipImage;
        }

        public void setBarcodeScanIsOn(string x)
        {
            if (x == "0") BarcodeScannerIsOn = false;
            if (x == "1") BarcodeScannerIsOn = true;
        }

        public void setFingerScanIsOn(string x)
        {
            if (x == "0") FingerScannerIsOn = false;
            if (x == "1") FingerScannerIsOn = true;
        }

        public void setFlipCamera(string x)
        {
            if (x == "0") flipImage = false;
            if (x == "1") flipImage = true;
        }

        public bool getBarcodeScanIsOn()
        {
            return BarcodeScannerIsOn;
        }

        public bool getFingerScanIsOn()
        {
            return FingerScannerIsOn;
        }

        public bool getFlipCamera()
        {
            return flipImage;
        }
        #endregion

        public void runPythonCountScript()
        {
            try
            {
                string fn = Path.GetFullPath("pythonInitial.pyc");
                var info = new ProcessStartInfo("python", "\""+fn+"\"");

                info.RedirectStandardInput = false;
                info.RedirectStandardOutput = true;
                info.UseShellExecute = false;
                info.CreateNoWindow = true;
                pyCountProcess = new Process();
                pyCountProcess.StartInfo = info;
                pyCountProcess.Start();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Script failed: " + ex.Message);
            }
        }

        public bool getFeedView()
        {
            return feedView;
        }
        public void setFeedView(bool fv)
        {
            feedView = fv;
        }

        public void readInParams()
        {
            using (StreamReader sr = new StreamReader("paramData.txt"))
            {
                string line = sr.ReadLine();
                string[] load = line.Split(',');
                setBarcodeScanIsOn(load[1]);

                line = sr.ReadLine();
                load = line.Split(',');
                setFingerScanIsOn(load[1]);

                line = sr.ReadLine();
                load = line.Split(',');
                setFlipCamera(load[1]);
            }
        }

        //database functions--------------------------------------
        public void clearDatabase()
        {
            //empty out what is currently loaded
            list.Clear();
            length = 0;
            //also clear the save file
            File.Delete(saveFileName); //delete works bc always remakes
        }
        private void LoadDatabase()
        {
            //pre: file exists
            //for each line, populate barcode, count, filename (could be none!!)
            string tempLine;
            string[] oneItemsData=new string[3];

            try
            {
                using (StreamReader sr = new StreamReader(saveFileName))
                {
                    while (!sr.EndOfStream) //while there's stuff in file
                    {
                        tempLine = sr.ReadLine();
                        oneItemsData = tempLine.Split(',');
                        add(oneItemsData[0], Convert.ToInt32(oneItemsData[1]), oneItemsData[2], Convert.ToDateTime(oneItemsData[3]));
                    }
                }
            }
            catch
            {
            }
        }

        public void add(string bar, int count, string filename, DateTime date)
        {
            list.Add(new InvItem(bar, count,filename, date)); //sorted by time added
            string written;
            try
            {
                using (StreamWriter sw = File.AppendText(saveFileName))
                {
                    written = bar + "," + count + ',' + filename + ',' + date.ToString();
                    sw.WriteLine(written);
                }
            }
            catch { }
            length++;
        }
        public string strAt(int index)
        {
            return list.ElementAt(index).toString();
        }
        public string strDataAt(int index)
        {
            return list.ElementAt(index).toDataString();
        }
        public Image getImageAt(int index)
        {
            return list.ElementAt(index).getPic();
        }
        public int size()
        {
            return length;
        }

        private static Inventory instance = new Inventory(); //pre-instantiated
        public static Inventory Instance => instance; //singleton pattern--property
        
        protected class InvItem
        {
            //an invitem should include all necessary params to identify a pill bottle
            //barcode, image
            //also pull from NDC database to find things like
            //pill shape, color, name, strength, etc
            //but for now just barcode and image

            private string barcode;
            private int count;
            private string picFilename;
            private DateTime date;

            public InvItem(string bar, int cnt, string filename, DateTime dat)
            {
                barcode = bar;
                count = cnt;
                picFilename = filename;
                date =dat;
            }
            
            public Image getPic() //no setters, but getters for anything else
            {
                if (!File.Exists(picFilename))
                {
                    return null;
                }
                return Image.FromFile(picFilename);
            }

            public string toString()
            {
                return "Barcode: " + barcode + ".\tCount: " + count + ".\tFilled: "+date.ToString(); //tabbed
            }

            public string toDataString()
            {
                return "Barcode: " + barcode + ".\nCount: " + count + ".\nFilled: " + date.ToString(); //new lined
            }
        }
    }
}
